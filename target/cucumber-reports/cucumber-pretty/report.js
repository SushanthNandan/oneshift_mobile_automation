$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/Features/UploadLater.feature");
formatter.feature({
  "line": 1,
  "name": "Upload later flow",
  "description": "",
  "id": "upload-later-flow",
  "keyword": "Feature"
});
formatter.before({
  "duration": 37480404700,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Report a problem in Finished loading screen",
  "description": "",
  "id": "upload-later-flow;report-a-problem-in-finished-loading-screen",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "the app is Installed and login as shippercarrier",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I verify Sign In Title for app",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I create duplicate shipment in web page",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I assign carrier and dispatch",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I verify Dispatched status in logs",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I login to app",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I verify app is in home page",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I click on Begin Shipment",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I verify app is in \"FIRST PICK\" page",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I select first pick/drop",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "I verify i am in \"CHECKED IN\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "I click on confirm button in checked in screen",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I verify i am in \"FINISHED LOADING\" finish screen",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click on confirm button in finished loading screen",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I verify app is in \"COMPLETE\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "I click on next in pick/drop complete screen",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I verify app is in \"NEXT DESTINATION\" page",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "I select first pick/drop",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I verify i am in \"CHECKED IN\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "I click on confirm button in checked in screen",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I verify i am in \"FINISHED UNLOADING\" finish screen",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I click on upload POD in finished unloading screen",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I click on Upload Later",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "I verify app is in \"COMPLETE\" screen",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "I click on next in pick/drop complete screen",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I verify POD added status not added in logs",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "I verify POD pending in home page",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "I click on Upload pending POD",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "I select to upload image",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "I capture photo",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "I select captured photo",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "I upload photo",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "I verify POD added status in logs",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "I Complete the shipment successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 39,
  "name": "I verify shipment completed status in logs",
  "keyword": "Then "
});
formatter.match({
  "location": "Login_stepDef.the_app_Installed_and_login_as_shipper_carrier()"
});
formatter.result({
  "duration": 9730381800,
  "status": "passed"
});
formatter.match({
  "location": "Login_stepDef.i_verify_sign_in_title_app()"
});
formatter.result({
  "duration": 516784100,
  "status": "passed"
});
formatter.match({
  "location": "WeCreateShipment_stepDef.create_duplciate_shipment()"
});
formatter.result({
  "duration": 26190296400,
  "status": "passed"
});
formatter.match({
  "location": "WeCreateShipment_stepDef.assign_carrier_and_dispatch()"
});
formatter.result({
  "duration": 19205274200,
  "status": "passed"
});
formatter.match({
  "location": "Weblogstatus_stepdef.dispatched()"
});
formatter.result({
  "duration": 581078800,
  "status": "passed"
});
formatter.match({
  "location": "Login_stepDef.Login()"
});
formatter.result({
  "duration": 19019583700,
  "status": "passed"
});
formatter.match({
  "location": "Login_stepDef.verify_homepage()"
});
formatter.result({
  "duration": 411956000,
  "status": "passed"
});
formatter.match({
  "location": "homePage_stepDef.begin_shipment()"
});
formatter.result({
  "duration": 21002505400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "FIRST PICK",
      "offset": 20
    }
  ],
  "location": "homePage_stepDef.verify_title(String)"
});
formatter.result({
  "duration": 11947420400,
  "error_message": "java.lang.AssertionError: Driver has not began shipment expected [true] but found [false]\r\n\tat org.testng.Assert.fail(Assert.java:96)\r\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\r\n\tat org.testng.Assert.assertTrue(Assert.java:44)\r\n\tat Pages.homePAgeObjects.verify_firstPick_title(homePAgeObjects.java:98)\r\n\tat StepDefinition.homePage_stepDef.verify_title(homePage_stepDef.java:32)\r\n\tat ✽.Then I verify app is in \"FIRST PICK\" page(src/test/java/Features/UploadLater.feature:13)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "CompletesShipment_stepdef.selectpick()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "CHECKED IN",
      "offset": 18
    }
  ],
  "location": "CompletesShipment_stepdef.verify_checkin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.confirm_checkin()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "FINISHED LOADING",
      "offset": 18
    }
  ],
  "location": "CompletesShipment_stepdef.verify_finishloading(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.confirm_finish()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "COMPLETE",
      "offset": 20
    }
  ],
  "location": "CompletesShipment_stepdef.verify_completeScreen(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.click_next()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "NEXT DESTINATION",
      "offset": 20
    }
  ],
  "location": "homePage_stepDef.verify_title(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.selectpick()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "CHECKED IN",
      "offset": 18
    }
  ],
  "location": "CompletesShipment_stepdef.verify_checkin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.confirm_checkin()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "FINISHED UNLOADING",
      "offset": 18
    }
  ],
  "location": "CompletesShipment_stepdef.verify_finishloading(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.uploadPOD()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.upload_later()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "COMPLETE",
      "offset": 20
    }
  ],
  "location": "CompletesShipment_stepdef.verify_completeScreen(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.click_next()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Weblogstatus_stepdef.noPODadded()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "homePage_stepDef.verify_PendingPOD()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "homePage_stepDef.uploadPOD_home()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.select_uploadimage()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.capture_photo()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.select_photo()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.upload_image()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Weblogstatus_stepdef.verify_PODAdded()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CompletesShipment_stepdef.complete_Shipment()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Weblogstatus_stepdef.verify_completedstatus()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 3667594000,
  "status": "passed"
});
});