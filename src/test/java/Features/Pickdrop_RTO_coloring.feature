Feature: Pick_Drop functionality

		
Scenario: Story6342, Story6082 Combine Pick & Drop Section and Show RTO Symbol in Pick 1
	Given the app is Installed and login as shippercarrier
	Then I verify Sign In Title for app
	When I create duplicate shipment in web page
	And I disable Capture Commodity Images	
	And I enable Return to Origin  	
	And I assign carrier and dispatch 	
	Then I verify Dispatched status in logs
	When I login to app
	Then I verify app is in home page
	When I click on Begin Shipment
	Then I verify the colouring of pickdrop "#4c4c4c" displayed
	When I select first pick/drop	
	Then I verify i am in "CHECKED IN" screen 
	When I click on confirm button in checked in screen
	Then I verify i am in "FINISHED LOADING" finish screen
	And I verify the colouring of pickdrop "#3176b7" displayed
	When I click on confirm button in finished loading screen
	And I click on next in pick/drop complete screen	
	Then I verify app is in "SELECT YOUR DESTINATION" page
	When I select first pick/drop
	Then I verify i am in "CHECKED IN" screen
	When I click on confirm button in checked in screen
	Then I verify i am in "FINISHED UNLOADING" finish screen
	When I click on upload POD in finished unloading screen
	And I select to upload image
	When I capture photo
	And I select captured photo
	When I upload photo
	When I click on next in pick/drop complete screen
	Then I verify the colouring of pickdrop "#309804" displayed
	Then I verify the colouring of RTO "#4c4c4c" displayed
	When I select first pick/drop	
	Then I verify i am in "ARRIVED ?" screen 
	Then I verify the colouring of pickdrop "#3176b7" displayed
	When I click on confirm button in checked in screen
	Then I verify the colouring of pickdrop "#309804" displayed
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
	
	
	
		