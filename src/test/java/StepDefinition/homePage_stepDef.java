package StepDefinition;

import cucumber.api.java.en.*;

import static org.junit.Assert.assertTrue;

import com.google.common.base.CharMatcher;

import Pages.WebLoginPage;
import Pages.homePAgeObjects;
import Utilities.BaseClass;



public class homePage_stepDef {
	protected homePAgeObjects homepageobjects = new homePAgeObjects(BaseClass.getDriver());


	@When("^I click on Begin Shipment$")
	public void begin_shipment() throws InterruptedException {
		homepageobjects.click_beginShipment();
		System.out.println("Begin shipment started");
		Thread.sleep(10000);
	}
	@When("^I allow location permissions$")
	public void allow_location_permission() throws InterruptedException {
		homepageobjects.Enable_permission();
	}

	@Then("^I verify app is in \"(.*)\" page$")
	public void verify_title(String exptitle) {
		homepageobjects.verify_firstPick_title(exptitle);

	}



	@Then("^I verify POD pending in home page$")
	public void verify_PendingPOD() throws InterruptedException {
		Thread.sleep(10000);
		homepageobjects.sync_pending();
	}

	@When("^I click on Upload pending POD$")
	public void uploadPOD_home() {
		homepageobjects.UploadPending_POD();
		System.out.println("Upload POD screen is displayed");
	}


	@When("^I click on menu$")
	public void navigate_menu() {
		homepageobjects.clikc_menu();
	}

	@Then("^I verify dispatch number in phone$")
	public void verify_dispatchnum() {
		homepageobjects.clikc_menu();
		homepageobjects.get_dispatchnum();
		@SuppressWarnings("unused")
		String web_dispatchnum = CharMatcher.inRange('0', '9').retainFrom(WebLoginPage.webdispatch_number);
		//		@SuppressWarnings("unused")
		String mobile_dispatchnum = CharMatcher.inRange('0', '9').retainFrom(homepageobjects.mobile_dispatchnum);
		System.out.println("Web num"+web_dispatchnum);
		System.out.println("mobile num"+mobile_dispatchnum);
		assertTrue(mobile_dispatchnum.contains(web_dispatchnum));

	}


}
