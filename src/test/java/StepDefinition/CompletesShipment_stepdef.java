package StepDefinition;

import cucumber.api.java.en.*;
import Utilities.BaseClass;
import Pages.WebCreateShipment;
import Pages.completeShipmentPages;


public class CompletesShipment_stepdef {
	protected static completeShipmentPages completeshipments_pages = new completeShipmentPages(BaseClass.getDriver());

	@When("^I select first pick/drop$")
	public void selectpick() throws InterruptedException {
		completeshipments_pages.select_availablePickDrop();
		System.out.println("Driver started pick/drop");
	}

	@When("^I select first pick/drop in Off Duty$")
	public void selectPick_offDuty() throws InterruptedException {
		completeshipments_pages.select_PickDrop_offduty();
	}

	@And("^I enable On duty from popup$")
	public void enableOnduty() {
		completeshipments_pages.enable_now();
	}

	@Then("^I verify i am in \"(.*)\" screen$")
	public static void verify_checkin(String exptext) {
		completeshipments_pages.verify_checkin_text(exptext);
		System.out.println("App is in "+exptext+"  screen");

	}

	@When("^I click on confirm button in checked in screen$")
	public static void confirm_checkin() {
		completeshipments_pages.click_confirm_checkin();
		System.out.println("Check IN confirmed");
	}

	@Then("^I verify app is in commodity \"(.*)\" page$")
	public void verify_commodity_screen(String exptitle) {
		completeshipments_pages.Commodity_images_screen(exptitle);

	}
	@Then("^I verify i am in \"(.*)\" finish screen$")
	public static void verify_finishloading(String exptitle) {
		completeshipments_pages.verify_finishloading_title(exptitle);
		System.out.println("User is in "+exptitle+" screen");
	}

	@When("^I click on confirm button in finished loading screen$")
	public static void confirm_finish() throws InterruptedException {
		completeshipments_pages.click_finish_checkin();
		System.out.println("Finished Loading");
	}

	@Then("^I verify app is in \"(.*)\" screen$")
	public static void verify_completeScreen(String exptitle) {
		completeshipments_pages.verify_Complete_Title(exptitle);
		System.out.println("Completed pick/drop");


	}

	@When("^I click on next in pick/drop complete screen$")
	public static void click_next() {
		completeshipments_pages.Click_next_button();	

	}

	@When("^I click on upload POD in finished unloading screen$")
	public static void uploadPOD() {
		completeshipments_pages.click_upload_POD();	
		System.out.println("Driver is in Upload POD screen");

	}

	@And("^I select to upload image$")
	public static void select_uploadimage() {
		completeshipments_pages.Select_upload_image();		
	}

	@When("^I capture photo$")
	public static void capture_photo() {
		//completeshipments_pages.allow_permissions();
		completeshipments_pages.Camera_photo();
	}

	@And("^I cancel captured photo$")
	public static void cancel_photo() {
		completeshipments_pages.Cancel_Captured_photo();
	}
	@And("^I select captured photo$")
	public static void select_photo() {
		completeshipments_pages.Select_Captured_photo();
	}

	@When("^I upload photo$")
	public static void upload_image() throws InterruptedException {
		completeshipments_pages.Upload_selected_POD();
	}

	@When("^I click on Report a problem$")
	public void select_rap() {
		completeshipments_pages.rap_checkin();		
	}

	@And("^I choose issue type as \"(.*)\" in rap$")
	public void choose_option(String option) {
		completeshipments_pages.choose_option();
		completeshipments_pages.select_issue(option);
	}

	@And("^I select issue in Report a problem as no impact$")
	public void noimpact_issue() {
		completeshipments_pages.no_impact();
		completeshipments_pages.next_rap();
	}

	@And("^I select issue in Report a problem as willnot make$")
	public void willnotmake_issue() {
		completeshipments_pages.willnot_make();
		completeshipments_pages.next_rap();
	}

	@And("^I select issue in Report a problem as running late$")
	public void runninglate_issue() {
		completeshipments_pages.running_late();
		completeshipments_pages.next_rap();

	}

	@When("^I upload problem in Report a problem$")
	public void upload_rap() {
		completeshipments_pages.Select_upload_image();
		completeshipments_pages.allow_permissions();
		completeshipments_pages.Camera_photo();
		completeshipments_pages.usephoto_rap();
		completeshipments_pages.Report();

	}

	@When("^I click on Upload Later$")
	public void upload_later() {
		completeshipments_pages.uploadLater();
	}

	@When("^I click on Report a problem in loading page$")
	public void rap_loading() {
		completeshipments_pages.rap_loading();
	}

	@And("^I change driver state to OFF Duty$")
	public void enble_offDuty() {
		completeshipments_pages.go_offDuty();
	}

	@Then("^I Complete the shipment successfully$")
	public void complete_Shipment() {
		completeshipments_pages.Complete_shipment();
	}

	@And("^I verify the tracker page$")
	public void verify_tracker_page() {
		completeshipments_pages.Verify_scheduled_date();
		completeshipments_pages.Verify_addressline();
		completeshipments_pages.Verify_addresscity();

	}

	@And("^I select \"(.*)\" screen$")
	public void click_shipment_details(String exptext) 	{
		completeshipments_pages.shipmentdetails_screen(exptext);
		System.out.println("Shipment details screen is displayed");
	}

	@Then("verify \"(.*)\" title$")
	public void commodity_images(String exptext) {
		completeshipments_pages.verify_commmodity_images_text(exptext);
		System.out.println("Require Commodity Images title is displayed");

	}

	@When("^I close shipment details screen$")
	public void close_shipment_details_screen() {
		completeshipments_pages.close_shipmentdetails();
	}




	@And("^I verify pick company name$") 
	public void pick_companyName() {
		completeshipments_pages.verify_pick_companyName();
	}

	@And("^I verify drop company name$") 
	public void drop_companyName() {
		completeshipments_pages.verify_drop_companyName();
	}

	@When("^I select back icon$")
	public void select_back_icon() {
		completeshipments_pages.navigate_to_previous_screen();
	}

	@Then("^I verify the sync screen in app$")
	public void verify_syncScreen() {
		completeshipments_pages.verifySyncScreen();
		System.out.println("Sync screen is displayed successully");
	}
	
	
	@And("^I verify the PopUp notification with pick name \"(.*)\" and drop name \"(.*)\" in app$")
	public void verify_the_popup_notification(String pickname, String dropname) {
		completeshipments_pages.verify_popup_notification(pickname, dropname);
	}
	
}
