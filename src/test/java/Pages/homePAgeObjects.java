package Pages;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Utilities.BaseClass;
import Utilities.CommonLib;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class homePAgeObjects {
	static AppiumDriver<MobileElement> androiddriver = BaseClass.getDriver();
	public static String mobile_dispatchnum = "";

	

	@AndroidFindBy(xpath="//*[@text='ENABLE']")
	static MobileElement enable;
	@AndroidFindBy(xpath="//*[@text='ALLOW']")
	static MobileElement allow;
	@AndroidFindBy(xpath="//*[@content-desc='ShipmentCard-Content-BeginShipmentButton']")
	static List<MobileElement> begin_shipment;
	@AndroidFindBy(xpath="//*[@content-desc='ItemsScreen-Content-Title']")
	static MobileElement firstPick_title;
	@AndroidFindBy(xpath="//*[@content-desc='Login-SignInButton']")
	static MobileElement Shipment_title;
	@AndroidFindBy(xpath="//*[@content-desc='MainHeader-Header-Body-Title]")
	static MobileElement active_noshipment_button;
	@AndroidFindBy(xpath="//*[@text='UPCOMING SHIPMENTS']")
	static MobileElement upcoming_shipments;
	@AndroidFindBy(xpath="//*[@content-desc='ListButton-Recent shipments (0)-Text']")
	static MobileElement recent_shipments;
	@AndroidFindBy(xpath="//*[@content-desc='Greetings-Root-DayTime']")
	static MobileElement greetings_msg;
	@AndroidFindBy(xpath="//*[@content-desc='ShipmentCard-Content-BeginShipmentButton']")
	static MobileElement beginshipment;
	@AndroidFindBy(xpath="//*[@content-desc='MainHeader-Header-Left-MenuView-Icon']")
	static MobileElement menu;
	@AndroidFindBy(xpath="//*[@content-desc='MainHeader-OnDutyChanged']")
	static MobileElement toggle;
	@AndroidFindBy(xpath="//*[@content-desc='ShipmentCard-Card-Body']")
	static MobileElement shipments;
	@AndroidFindBy(xpath="//*[@content-desc='UploadPhotoScreen-onUploadPhotosLater']")
	static MobileElement driver_name;
	@AndroidFindBy(xpath="//*[@content-desc='ShipmentCard-Card-Body-Content-SyncPendingText']")
	static MobileElement sync_pending;	 
	@AndroidFindBy(xpath="//*[@content-desc='ShipmentCard-Card-Body-CollapsibleToggleButton-Text']")
	static MobileElement shipmentdetails_collapse;
	@AndroidFindBy(xpath="//*[@content-desc='ShipmentItem-UploadPodButton-Text']")
	static MobileElement uploadPOD_home;
	@AndroidFindBy(xpath="//*[@content-desc='SidebarItem-PhoneText']")
	static List<MobileElement> dispatchnum;	 
	@AndroidFindBy(xpath="//*[@content-desc='MainHeader-Header-Left-MenuView']")
	static List<MobileElement> burgermenu;	
		
	public homePAgeObjects(AppiumDriver<MobileElement> androiddriver) {
		PageFactory.initElements(new AppiumFieldDecorator(androiddriver), this);
		homePAgeObjects.androiddriver = androiddriver;

	}

	public void click_beginShipment() {
		CommonLib.scrollAndClick(WebCreateShipment.shipmentID);
		CommonLib.waitforelement(androiddriver, beginshipment, 20);
		if(begin_shipment.size()>1)
			begin_shipment.get(begin_shipment.size()-1).click();
		else
			begin_shipment.get(0).click();
	}

	public void Enable_permission() throws InterruptedException {
		//      CommonLib.waitforelement(androiddriver, enable, 20);
		//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(enablePath)));
		try{
			if(enable.isDisplayed())
			{
				enable.click();
				allow.click();
			}
		}
		catch(Exception e){
			System.out.println("Warning: No Element found");
		}
	}

	public void verify_firstPick_title(String exptext) {
		androiddriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, firstPick_title, 30);
		String actText = firstPick_title.getText();
		Assert.assertTrue(actText.contains(exptext), "Driver has not began shipment");
	}
	
	

	public void get_shipments_availability() {
		String status = active_noshipment_button.getText();
		System.out.println("Shipments status is ........."+status);
	}

	public void sync_pending() {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		sync_pending.isDisplayed();
	}

	public void UploadPending_POD() {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		shipmentdetails_collapse.click();
		for(int i =0; i<5;i++) {
			CommonLib.scroll();
		}
		uploadPOD_home.click();
	}

	public void clikc_menu() {
		menu.click();
	}

	public void get_dispatchnum() {
		mobile_dispatchnum = dispatchnum.get(0).getText();
	}

}
