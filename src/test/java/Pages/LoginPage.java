package Pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import Utilities.BaseClass;
import Utilities.CommonLib;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import properties.Configprops;


public class LoginPage{
	static AppiumDriver<MobileElement> androiddriver;


	@AndroidFindBy(xpath="//*[@content-desc='Login-Title']")
	MobileElement Signin_Title;
	@AndroidFindBy(xpath="//*[@content-desc='Login-ForgotPassword-Text']")
	static MobileElement forgot_password;
	@AndroidFindBy(xpath="//*[@content-desc='Login-PrivacyButton']")
	static MobileElement privacy_login;
	@AndroidFindBy(xpath = "//*[@content-desc='Login-Form-MobileInput']")				   
	static MobileElement number_input;
	@AndroidFindBy(xpath = "//*[@content-desc='Login-Form-PasswordInput']")
	static MobileElement password_input;
	@AndroidFindBy(xpath="//*[@content-desc='Login-SignInButton']")
	static MobileElement SignIn_button;
	@AndroidFindBy(xpath="//*[@content-desc='Login-Form-ErrorView-Text']")
	static MobileElement Login_error_txt;
	@AndroidFindBy(xpath="//*[@content-desc='MainHeader-Header']")
	static MobileElement homepage_header;
	@FindBy(xpath="//*[@text='UPDATE']")
	public static MobileElement update;
	@FindBy(xpath="//*[@text='PODS ARE MISSING']")
	public static MobileElement pods_are_missing;


	public LoginPage(AppiumDriver<MobileElement> androiddriver) {
		PageFactory.initElements(new AppiumFieldDecorator(androiddriver), this);
		LoginPage.androiddriver = androiddriver;
	}


	public void Verify_Signin_Title() throws InterruptedException {
		//		boolean present = androiddriver.findElement(By.xpath(Signin_Title)).isDisplayed();
		//		System.out.println(Signin_Title);
		boolean present = Signin_Title.isDisplayed();
		Assert.assertTrue(present,"Sign in title is not verified");
	}

	public void Login() throws InterruptedException {
		number_input.click();
		androiddriver.navigate().back();
		number_input.sendKeys(Configprops.phonenumber);
		CommonLib.waitforelement(androiddriver, password_input, 10);
		password_input.click();
		androiddriver.navigate().back();
		password_input.sendKeys(Configprops.password);
		androiddriver.navigate().back();
		SignIn_button.click();
		Thread.sleep(10000);

	}

	public void verify_homePage() throws InterruptedException {
		//AndroidElement menu = (AndroidElement) androiddriver.findElementByAccessibilityId("MainHeader-Header-Left-MenuView");
		CommonLib.waitforelement(androiddriver, homepage_header, 10);
		Assert.assertTrue(homepage_header.isDisplayed(),"App is not logged in successfully");
		System.out.println("App is logged in successfully");
		/*
		 * if(pods_are_missing.isDisplayed()) {
		 * System.out.println("Complete the previous shipment"); androiddriver.quit(); }
		 */	
	}

	public void update_app() 
	{
		if(update.isDisplayed())
			update.click();
		else
			System.out.println("App is already up to date");

	}

}
