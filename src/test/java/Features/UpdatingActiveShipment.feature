Feature: UpdatingActiveShipment Flow
	

Scenario: Story-6271, Story-6036 Pick/Drop & Commodity Save and notify driver behavior
	Given the app is Installed and login as shippercarrier
	Then I verify Sign In Title for app
	When I create duplicate shipment in web page
	And I disable Capture Commodity Images	
	And I assign carrier and dispatch 	
	Then I verify Dispatched status in logs
	When I login to app
	Then I verify app is in home page
	When I click on Begin Shipment
	Then I verify app is in "SELECT YOUR DESTINATION" page	
	When I update pick name "AUTOMATION" and drop name "TEST" in web 
	Then I verify the sync screen in app
	And I verify the PopUp notification with pick name "AUTOMATION" and drop name "TEST" in app 
	When I select first pick/drop	
	Then I verify i am in "CHECKED IN" screen 
	When I click on confirm button in checked in screen
	Then I verify i am in "FINISHED LOADING" finish screen
	When I click on confirm button in finished loading screen
	And I click on next in pick/drop complete screen	
	Then I verify app is in "SELECT YOUR DESTINATION" page
	When I select first pick/drop
	Then I verify i am in "CHECKED IN" screen
	When I click on confirm button in checked in screen
	Then I verify i am in "FINISHED UNLOADING" finish screen
	When I click on upload POD in finished unloading screen
	And I select to upload image
	When I capture photo
	And I select captured photo
	When I upload photo
	When I click on next in pick/drop complete screen
	
	
	

	
		