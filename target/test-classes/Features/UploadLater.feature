Feature: Upload later flow


Scenario: Report a problem in Finished loading screen
	Given the app is Installed and login as shippercarrier
	Then I verify Sign In Title for app
	When I create duplicate shipment in web page
	And I disable Capture Commodity Images	
	And I assign carrier and dispatch 	
	Then I verify Dispatched status in logs
	When I login to app
	Then I verify app is in home page
	When I click on Begin Shipment
	Then I verify app is in "SELECT YOUR DESTINATION" page
	Then I verify app is in "FIRST PICK" page
	When I select first pick/drop
	Then I verify i am in "CHECKED IN" screen
	And I click on confirm button in checked in screen
	Then I verify i am in "FINISHED LOADING" finish screen
	And I click on confirm button in finished loading screen
	Then I verify app is in "COMPLETE" screen
	And I click on next in pick/drop complete screen
	Then I verify app is in "NEXT DESTINATION" page
	When I select first pick/drop
	Then I verify i am in "CHECKED IN" screen
	And I click on confirm button in checked in screen
	Then I verify i am in "FINISHED UNLOADING" finish screen
	And I click on upload POD in finished unloading screen
	When I click on Upload Later
	Then I verify app is in "COMPLETE" screen
	And I click on next in pick/drop complete screen	
	Then I verify POD added status not added in logs
	Then I verify POD pending in home page
	When I click on Upload pending POD
	And I select to upload image
	And I capture photo
	And I select captured photo
	When I upload photo
	Then I verify POD added status in logs
	Then I Complete the shipment successfully
	Then I verify shipment completed status in logs
	
	
	
	
	
	
	
		