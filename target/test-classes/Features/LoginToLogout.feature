@Sanity
Feature: Login Flow


Scenario: TC_3585 Begin Shipment when Online
	Given the app is Installed
	Then I verify Sign In Title for app
	When I create shipment in web page	
	And I accept assign the shipment
	Then I verify Dispatched status in logs
	When I login to app
	When I allow location permissions
	Then I verify app is in home page
	When I click on Begin Shipment
	Then I verify app is in "SELECT YOUR DESTINATION" page
	Then I verify driver started status in logs
	When I select first pick/drop	
	Then I verify i am in "CHECKED IN" screen
	When I click on confirm button in checked in screen
	Then I verify In Transit status in logs
	Then I verify i am in "FINISHED LOADING" finish screen
	Then I verify checked in status in logs
	When I click on confirm button in finished loading screen
	Then I verify finished loading status in logs
	When I click on next in pick/drop complete screen	
	Then I verify app is in "SELECT YOUR DESTINATION" page
	When I select first pick/drop
	Then I verify i am in "CHECKED IN" screen
	When I click on confirm button in checked in screen
	Then I verify i am in "FINISHED UNLOADING" finish screen
	Then I verify checked in status in logs
	When I click on upload POD in finished unloading screen
	And I select to upload image
	Then I verify finished loading status in logs
	When I capture photo
	And I select captured photo
	When I upload photo
	When I click on next in pick/drop complete screen
	Then I verify app is in "COMPLETE" screen
	Then I verify POD added status in logs
	Then I verify shipment completed status in logs
	Then I Complete the shipment successfully
	
		
	
	
	
	
	
	
	
		