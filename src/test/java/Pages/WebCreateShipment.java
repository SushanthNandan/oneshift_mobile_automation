package Pages;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;
import java.util.List;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Factory;
import org.testng.asserts.SoftAssert;

import Utilities.CommonFunctions;
import Utilities.CommonLib;
import Utilities.ObjectHelper;
import Utilities.XLHandler;
import cucumber.api.java.en.Then;
import properties.Configprops;
import Pages.WebLoginPage;
import Pages.ManageShipmentsPage;


public class WebCreateShipment {
	public WebDriver webdriver;
	public SoftAssert softassert = new SoftAssert();
	public static String shipmentID;
	public static String pick_companyName;
	public static String drop_companyName;
	int createshipment = 0;
	String[] shipmentdata = XLHandler.readexcel("19_S6_2886_1", 1, "NewTestData.xlsx");
	String[] shipmentdata1 = XLHandler.readexcel("19_S6_2886_1", 3, "NewTestData.xlsx");
	public String workingDir = System.getProperty("user.dir");


	@FindBy(xpath = "//b[text()='Add details']")
	public static WebElement addDetailsText;

	@FindBy(xpath = "//ul[@role='listbox']//li")
	public static List<WebElement> currencylist;

	@FindBy(xpath = "//span[contains(.,'Shipments')]")
	public static WebElement Shipments;

	@FindBy(xpath = "//p[contains(.,'Manage Shipments')]")
	public WebElement manageShipments;

	@FindBy(xpath = "//input[@id='1shift-ms-stf-shipmentId']")
	public WebElement shipmentId_filter;

	@FindBy(xpath = "//img[@alt='exception flag']")
	public WebElement exception_flag;

	@FindBy(xpath = "//span[text()='Create a New Shipment']")
	public WebElement createaNewShipment;

	@FindBy(xpath = "//span[text()='Appointment Needed']")
	public List<WebElement> appointmentNeddedlist;

	@FindBy(xpath = "//input[@placeholder='Input number here']")
	public static WebElement shipmentId;

	@FindBy(xpath = "//div[@name='trailer_category']//label")
	public static List<WebElement> trailerTypeList;

	@FindBy(xpath = "//input[@name='trailer_subcategory']//parent::span")
	public static List<WebElement> trailerSubTypeList;

	@FindBy(xpath = "//input[@type='checkbox']") // 1 to 3 Equipment
	public static List<WebElement> equipemntCheckBoxesList;

	@FindBy(xpath = "//input[@type='checkbox']") // 4 to 10 Services
	public static List<WebElement> servicesCheckBoxesList;

	@FindBy(xpath = "//input[@type='checkbox']") // 11 Cross border
	public static List<WebElement> crossBorderCheckBoxesList;

	@FindBy(xpath = "//input[@placeholder='Add requirements here']")
	public static WebElement specialRequirements;

	@FindBy(xpath = "//input[@placeholder='Add all-in rate here']")
	public static WebElement enterYourAllInRate;

	@FindBy(xpath = "//div[@id='select-rate_currency']")
	public static WebElement currecyfield;

	@FindBy(xpath = "//ul[@role='listbox']//li")
	public static List<WebElement> currency;

	@FindBy(xpath = "//span[text()='No Default Customs Broker']")
	public static WebElement noDefaultCustomsBroker;

	@FindBy(xpath = "//span[text()='Alternate Customs Broker']")
	public static WebElement alternateCustomsBroker;

	@FindBy(xpath = "//input[@name='name']")
	public static WebElement brokerName;

	@FindBy(xpath = "//input[@name='email']")
	public static WebElement brokerEmail;

	@FindBy(xpath = "//input[@name='phone']")
	public static WebElement brokerPhoneNumber;

	@FindBy(xpath = "//label[text()='PARS']")
	public static WebElement pars;

	@FindBy(xpath = "//label[text()='PAPS']")
	public static WebElement paps;

	@FindBy(xpath = "//span[text()='Next']")
	public static WebElement nextButton;

	@FindBy(xpath = "//span[text()='2. Picks & Drops']")
	public static WebElement secondPageText;

	@FindBy(xpath = "//a[text()='Edit']")
	public static WebElement edit1stPage;

	@FindBy(xpath = "//input[@name='description']")
	public static List<WebElement> description;

	@FindBy(xpath = "//input[@name='quantity']")
	public static List<WebElement> quntity;

	@FindBy(xpath = "//input[@name='quantityUnits']")
	public static List<WebElement> quantityUnits;

	@FindBy(xpath = "//input[@name='weight']")
	public static List<WebElement> weight;

	@FindBy(xpath = "//input[@name='weightUnits']")
	public static List<WebElement> weightUnits;

	@FindBy(xpath = "//input[@name='temperature']")
	public static List<WebElement> temprature;

	@FindBy(xpath = "//input[@name='temperatureUnits']")
	public static List<WebElement> tempratureUnits;

	@FindBy(xpath = "//input[@name='value']")
	public static List<WebElement> value;

	@FindBy(xpath = "//input[@name='valueCurrency']")
	public static List<WebElement> valueCurrency;

	@FindBy(xpath = "//span[text()='Save']")
	public static List<WebElement> addButton;

	@FindBy(xpath = "//span[text()='Cancel']")
	public static List<WebElement> cancelButton;

	@FindBy(xpath = "//span[text()='Edit']")
	public static List<WebElement> editButton;

	@FindBy(xpath = "//span[text()='Add Another Commodity']")
	public static WebElement addAnotherCommodityButton;

	@FindBy(xpath = "//input[@name='companyName']")
	public static List<WebElement> companyName;

	@FindBy(xpath = "//input[@name='addressLine1']")
	public static List<WebElement> address;

	@FindBy(xpath = "//label[@for='country']//parent::div")
	public static List<WebElement> country;

	@FindBy(xpath = "//ul[@role='listbox']//li")
	public static List<WebElement> countryList;

	@FindBy(xpath = "//input[@name='city']")
	public static List<WebElement> city;

	@FindBy(xpath = "//label[text()='State/Province']//parent::div")
	public static List<WebElement> state;

	@FindBy(xpath = "//ul[@role='listbox']//li")
	public static List<WebElement> stateList;

	@FindBy(xpath = "//input[@name='zip']")
	public static List<WebElement> postalCode;

	@FindBy(xpath = "//input[@name='number']")
	public static List<WebElement> pickUpNumber;

	@FindBy(xpath = "//input[@name='openHours']")
	public static List<WebElement> facilityOperatingFromTime;

	@FindBy(xpath = "//input[@name='closeHours']")
	public static List<WebElement> facilityOperatingToTime;

	@FindBy(xpath = "//div[text()='CAN+1']")
	public static List<WebElement> countryCode;

	@FindBy(xpath = "//ul[@role='listbox']//li")
	public static List<WebElement> countryCodeList;

	@FindBy(xpath = "//input[@name='phoneNumber']")
	public static List<WebElement> contactNumber;

	@FindBy(xpath = "//input[@name='notes']")
	public static List<WebElement> instruction;

	@FindBy(xpath = "//input[@name='pickDropDate']")
	public static List<WebElement> pickdate;

	@FindBy(xpath = "//input[@name='appointment_time']//parent::div/input")
	public static List<WebElement> appointmentTime;

	@FindBy(xpath = "//input[@name='appointment_confirmation_number']")
	public static List<WebElement> confirmation;

	@FindBy(xpath = "//label[text()='Name of the Commodity']//parent::div")
	public static List<WebElement> nameOfTheCommodity;

	@FindBy(xpath = "//ul[@role='listbox']//li")
	public static List<WebElement> nameOfTheCommodityList;

	@FindBy(xpath = "//button[@aria-label='Close']")
	public static WebElement uploadClose;

	@FindBy(xpath = "//img[@alt='delete']")
	public static List<WebElement> commodityAndbolDelete;

	@FindBy(xpath = "//span[text()='Add Another Pick']")
	public static WebElement addAnotherPick;

	@FindBy(xpath = "//*[@id='1shift-cs-pd-add-drop']")
	public static WebElement addAnotherDrop;

	@FindBy(xpath = "//div[text()='Select BOLs and other documents']")
	public static WebElement dropBOL;

	@FindBy(xpath = "//input[@type='checkbox']")
	public static List<WebElement> dropBOLListCheckBox;

	@FindBy(xpath = "//span[text()='Previous']")
	public static WebElement previousButton;

	@FindBy(xpath = "//span[text()='Create']")
	public static WebElement continueButton;

	@FindBy(xpath = "//span[text()='Shipment Successfully Created!']")
	public static WebElement shipmentSuccessMsg;

	@FindBy(xpath = "//span[text()='Request sent! Wait for Carrier to respond']")
	public static WebElement AssignedCarrierSuccessMsg;	

	@FindBy(xpath = "//span[text()='Shipment successfully assigned to you']")
	public static WebElement AssignedShipperCarrierSuccessMsg;


	@FindBy(xpath = "//p[contains(text(),'required')]")
	public static WebElement errorMsg;

	@FindBy(xpath = "//span[text()='c']")
	public static List<WebElement> addedcommodity;

	@FindBy(xpath = "//span[text()='Delete']")
	public static List<WebElement> deletebuttonlist;

	@FindBy(xpath = "//span[text()='Appointment Needed']")
	public static List<WebElement> appointmentNedded;

	@FindBy(xpath = "//label[text()='Quantity in #']")
	public static WebElement quantityLabel;

	@FindBy(xpath = "//div[text()='Commodity #1']")
	public static WebElement commodity1;

	@FindBy(xpath = "//div[text()='Commodity #2']")
	public static WebElement commodity2;

	// single commodity

	@FindBy(xpath = "//input[@name='weight']")
	public static WebElement c1weight;

	@FindBy(xpath = "//h6[contains(text(),'Edit')]")
	public static WebElement editCommodityText;

	@FindBy(xpath = "//span[text()='Save']")
	public static WebElement commoditySaveButton;

	@FindBy(xpath = "//span[text()='New Description']")
	public static WebElement editedDescription;

	@FindBy(xpath = "//input[@name='pickDropDate']")
	public static List<WebElement> pickDropDateList;

	@FindBy(xpath = "//span[text()='P']")
	public static WebElement P1;

	@FindBy(xpath = "//span[text()='D']")
	public static WebElement D1;

	@FindBy(xpath = "//span[text()='Dry Van']")
	public static WebElement vehicleName;

	@FindBy(xpath = "//label[@for='country']//parent::div//div//div//div")
	public static List<WebElement> selectedCountry;

	@FindBy(xpath = "//label[@for='state']//parent::div//div//div//div")
	public static List<WebElement> selectedState;

	@FindBy(xpath = "//label[text()='Name of the Commodity']//parent::div//div//div//div")
	public static List<WebElement> selectedCommodity;

	@FindBy(xpath = "//span[@id='client-snackbar']//following::button")
	public static WebElement successClose;

	@FindBy(xpath = "//div[label[text()='Quantity']]/div/input[@name='quantity']")
	public static List<WebElement> commodityQtyPickDrop;

	@FindBy(xpath = "//input[@type='file']")
	public static List<WebElement> pickBOLUpload;

	@FindBy(xpath = "//div[div[text()='Upload Fail - Incorrect file format']]")
	public static WebElement incorrectFileErrorMsg;

	@FindBy(xpath = "//div[div[text()='Upload complete']]")
	public static WebElement uploadCompleteMsg;

	@FindBy(xpath = "//button/span[text()='Dispatch']")
	public static WebElement dispatch_button;

	@FindBy(xpath = "//button/span[text()='Assign']")
	public static WebElement Assign_button;

	@FindBy(xpath = "//span[contains(text(),'Commodity')]")
	public static List<WebElement> commodityList;

	@FindBy(xpath = "//span[text()='P']")
	public static List<WebElement> P;

	@FindBy(xpath = "//span[text()='D']")
	public static List<WebElement> D;

	@FindBy(xpath = "//label[span[span[input[@checked]]]]/span[2]/span")
	public static WebElement selectedsubTrailerType;

	@FindBy(xpath = "//span[@class='selected-trailer-label']")
	public static WebElement selectedTrailerType;

	@FindBy(xpath = "//button[contains(@id,'1shift-cs-pd-pick-')]/span[text()='Save']")
	public static List<WebElement> pickSaveBtn;

	@FindBy(xpath = "//button[contains(@id,'1shift-cs-pd-drop-')]/span[text()='Save']")
	public static List<WebElement> dropSaveBtn;


	@FindBy(xpath = "//span[text()='Save']")
	public static List<WebElement> saveBtn;

	@FindBy(xpath = "//form[contains(@id,'1shift-cs-pd-pick')]/div[3]/div/div/div[label[text()='Quantity']]/div/input[@name='quantity']")
	public static List<WebElement> pickCommodityQtyInput;

	@FindBy(xpath = "//form[contains(@id,'1shift-cs-pd-drop')]/div[3]/div/div/div[label[text()='Quantity']]/div/input[@name='quantity']")
	public static List<WebElement> dropCommodityQtyInput;

	@FindBy(xpath = "//form[@id='1shift-cs-pd-pick-1-form']/div[3]/div/div[1]/div/div/div/div")
	public static List<WebElement> nameOfThePick1Commodity;

	@FindBy(xpath = "//form[@id='1shift-cs-pd-pick-2-form']/div[3]/div/div[1]/div/div/div/div")
	public static List<WebElement> nameOfThePick2Commodity;

	@FindBy(xpath = "//form[contains(@id,'1shift-cs-pd-drop')]/div[3]/div/div[1]/div")
	public static List<WebElement> nameOfTheDropCommodity;

	//Test Driver 2 or QA tst
	@FindBy(xpath="//li[text()='Automate driver']")
	public static WebElement DriverName;

	@FindBy(xpath = "//*[@id='1shift-rs-sd-edit-details']")
	public static WebElement edit_shippingdetails;

	@FindBy(xpath = "//*[@id='1shift-rs-sd-save-details']")
	public static WebElement save_shippingdetails;	

	@FindBy(xpath="//*[@id='1shift-rs-sds-capture-commodity-images']")
	public static WebElement commodityImages_toggleOFF;

	@FindBy(xpath="//*[@id='1shift-rs-sds-return-to-origin']")
	public static WebElement RTO_toggleOFF;

	@FindBy(xpath = "//*[@id='1shift-rs-sd-pick-0']//span[contains(text(), 'P')]/..")
	public static WebElement pick_color;

	@FindBy(xpath = "//*[@id='1shift-rs-sd-pick-0']//span[contains(text(), 'RTO')]/..")
	public static WebElement RTO_color;

	@FindBy(xpath = "//*[@id='1shift-rs-sd-edit-picks-drops']")
	public static WebElement edit_pickdropdetails;

	@FindBy(xpath = "//*[@id='ic_1shift_expand-all']")
	public static WebElement expand_all;

	@FindBy(xpath = "//*[@id='ic_1shift_collapse-all']")
	public static WebElement collapse_all;

	@FindBy(xpath = "//*[@id='1shift-rs-sd-picksDrops-save-edition']")
	public static WebElement save_pickdropdetails;



	public WebCreateShipment(WebDriver webdriver) {
		this.webdriver = webdriver;
		PageFactory.initElements(webdriver, this);
	}


	public void click_on_manage_shipments() throws Throwable {
		CommonFunctions.clickUsingJavaExecutor(Shipments);
		CommonFunctions.waitandClick(manageShipments, 10);
		if (CommonFunctions.waitForVisiblity(createaNewShipment, 30)) {
			softassert.assertEquals(true, true);
			System.out.println("Manage Shipments page is displayed");
		} else {
			softassert.assertEquals(false, true);
			System.out.println("Manage Shipments page is not displayed");
		}


	}

	public void duplicate_shipments() throws Exception {

		CommonFunctions.waitandNormalClick(ManageShipmentsPage.shipmentIdInput, 5);
		ManageShipmentsPage.shipmentIdInput.sendKeys("s11_343");
		Thread.sleep(3000);
		CommonFunctions.waitandClick(ManageShipmentsPage.dots, 30);
		CommonFunctions.clickUsingJavaExecutor(ManageShipmentsPage.duplicateShipmentBtn);
		shipmentID = shipmentdata[0] + CommonFunctions.getSaltString(5);
		shipmentId.sendKeys(shipmentID);
		CommonFunctions.waitandClick(nextButton, 30);
		CommonFunctions.waitUntilPageLoads();
		CommonFunctions.clickUsingJavaExecutor(saveBtn.get(0));
		CommonFunctions.waitForVisiblity(companyName.get(0), 10);
		pick_companyName = companyName.get(0).getAttribute("value");
		CommonFunctions.scrolltoElement(pickDropDateList.get(0));
		pickDropDateList.get(0).clear();
		pickDropDateList.get(0).sendKeys(CommonFunctions.getDate(0, "MM/dd/yyyy"));
		pickDropDateList.get(0).sendKeys(Keys.TAB);
		CommonFunctions.scrolltoElement(pickSaveBtn.get(0));
		CommonFunctions.waitandClick(pickSaveBtn.get(0), 30);
		CommonFunctions.waitForVisiblity(companyName.get(1), 10);
		drop_companyName = companyName.get(1).getAttribute("value");
		CommonFunctions.scrolltoElement(pickDropDateList.get(1));
		pickDropDateList.get(1).clear();
		pickDropDateList.get(1).sendKeys(CommonFunctions.getDate(0, "MM/dd/yyyy"));
		pickDropDateList.get(1).sendKeys(Keys.TAB);
		CommonFunctions.scrolltoElement(dropSaveBtn.get(0));
		CommonFunctions.waitandClick(dropSaveBtn.get(0), 30);
		CommonFunctions.scrolltoElement(addAnotherDrop);
		if (CommonFunctions.waitForVisiblity(addAnotherDrop, 30)) 
			softassert.assertEquals(true, true);
		else 		
			softassert.assertEquals(false, true, "Add another drop is not displayed");


	}



	public void click_on_create_new_shipment() throws Throwable {
		CommonFunctions.waitandClick(createaNewShipment, 30);
		if (CommonFunctions.waitForVisiblity(addDetailsText, 30)) {
			softassert.assertEquals(true, true);
			System.out.println("Create Shipments page is displayed");
		} else {
			softassert.assertEquals(false, true);
			System.out.println("Create Shipments page is not displayed");
		}

	}

	public void click_on_next() throws Throwable {
		shipmentID = shipmentdata[0] + CommonFunctions.getSaltString(5);
		shipmentId.sendKeys(shipmentID);
		trailerTypeList.get(3).click();
		CommonFunctions.waitandNormalClick(trailerSubTypeList.get(0), 30);
		equipemntCheckBoxesList.get(0).click();
		equipemntCheckBoxesList.get(1).click();
		servicesCheckBoxesList.get(5).click();
		servicesCheckBoxesList.get(6).click();
		specialRequirements.sendKeys(shipmentdata[1]);
		CommonFunctions.scrolltoElement(enterYourAllInRate);
		enterYourAllInRate.sendKeys(shipmentdata[2]);
		currecyfield.click();
		currencylist.get(1).click();
		CommonFunctions.waitandClick(nextButton, 30);
		if (CommonFunctions.waitForVisiblity(secondPageText, 30)) {
			softassert.assertEquals(true, true);
			System.out.println("Added Shipment details is displayed");

		} else {
			softassert.assertEquals(false, true);
		}

	}

	public void add_commodities_section() throws Throwable {
		description.get(0).sendKeys(shipmentdata[3]);
		quntity.get(0).sendKeys(shipmentdata[4]);
		quantityUnits.get(0).click();
		weight.get(0).sendKeys(shipmentdata[5]);
		CommonFunctions.scrolltoElement(weightUnits.get(1));
		weightUnits.get(1).click();
		temprature.get(0).sendKeys(shipmentdata[6]);
		tempratureUnits.get(0).click();
		value.get(0).sendKeys(shipmentdata[7]);
		valueCurrency.get(0).click();
		CommonFunctions.waitandClick(addButton.get(0), 30);


	}

	public void click_on_delete_pick() throws Throwable {
		CommonFunctions.waitandClick(deletebuttonlist.get(1), 30);
		if (CommonFunctions.waitForVisiblity(addAnotherPick, 30)) {
			assertEquals(true, true);
		} else {
			assertEquals(false, true);
		}

	}

	public void validate_Address_err_msg() throws Throwable {
		CommonFunctions.waitandClick(addAnotherPick, 30);

		if (CommonFunctions.waitForVisiblity(companyName.get(0), 30)) {
			assertEquals(true, true);
		} else {
			assertEquals(false, true);
		}

	}

	public void add_pick() throws Throwable {

		address.get(0).sendKeys(shipmentdata[9]);
		companyName.get(0).sendKeys(shipmentdata[8]);
		country.get(0).click();
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[10] + "')]");

		Thread.sleep(10);
		city.get(0).sendKeys(shipmentdata[11]);
		CommonFunctions.waitandNormalClick(state.get(0), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[12] + "')]");
		postalCode.get(0).sendKeys(shipmentdata[13]);
		CommonFunctions.scrolltoElement(pickUpNumber.get(0));
		pickUpNumber.get(0).sendKeys(shipmentdata[14]);

		CommonFunctions.scrolltoElement(facilityOperatingFromTime.get(0));
		facilityOperatingFromTime.get(0).sendKeys(shipmentdata[15]);
		facilityOperatingToTime.get(0).sendKeys(shipmentdata[16]);

		CommonFunctions.scrolltoElement(contactNumber.get(0));
		contactNumber.get(0).sendKeys(shipmentdata[17]);
		CommonFunctions.scrolltoElement(facilityOperatingFromTime.get(0));
		CommonFunctions.scrolltoElement(instruction.get(0));
		instruction.get(0).sendKeys(shipmentdata[18]);
		CommonFunctions.scrolltoElement(pickDropDateList.get(0));
		pickDropDateList.get(0).click();
		pickdate.get(0).sendKeys(shipmentdata[19]);
		CommonFunctions.waitandClick(appointmentNeddedlist.get(0), 30);
		CommonFunctions.scrolltoElement(nameOfTheCommodity.get(0));
		nameOfTheCommodity.get(0).click();
		CommonFunctions.waitandNormalClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.waitandClick(addButton.get(0), 30);
		//		if (CommonFunctions.waitForVisiblity(addedcommodity.get(1), 30)) {
		//			assertEquals(true, true);
		//		} else {
		//			assertEquals(false, true);
		//		}
		//
	}

	public void add_another_commodity() throws Throwable {
		addAnotherCommodityButton.click();
		CommonFunctions.scrolltoElement(description.get(1));
		description.get(1).sendKeys("Commodity 2");
		CommonFunctions.scrolltoElement(quntity.get(1));
		quntity.get(1).sendKeys("10");
		CommonFunctions.scrolltoElement(quantityUnits.get(3));
		quantityUnits.get(3).click();
		CommonFunctions.scrolltoElement(weight.get(1));
		weight.get(1).sendKeys("10");
		CommonFunctions.scrolltoElement(weightUnits.get(3));
		weightUnits.get(3).click();
		CommonFunctions.scrolltoElement(temprature.get(1));
		temprature.get(1).sendKeys("10");
		CommonFunctions.scrolltoElement(tempratureUnits.get(3));
		tempratureUnits.get(3).click();
		CommonFunctions.scrolltoElement(value.get(1));
		value.get(1).sendKeys("10");
		CommonFunctions.scrolltoElement(valueCurrency.get(2));
		valueCurrency.get(2).click();
		CommonFunctions.scrolltoElement(addButton.get(0));
		CommonFunctions.waitandClick(addButton.get(0), 30);
		if (CommonFunctions.waitForVisiblity(addedcommodity.get(0), 30)) {
			assertEquals(true, true);
		} else {
			assertEquals(false, true);
		}

	}


	public void add_drop() throws Throwable {
		CommonFunctions.scrolltoElement(address.get(1));
		address.get(1).sendKeys(shipmentdata[22]);
		companyName.get(1).sendKeys(shipmentdata[21]);
		country.get(1).click();
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[23] + "')]");
		Thread.sleep(10);
		city.get(1).sendKeys(shipmentdata[24]);
		CommonFunctions.waitandNormalClick(state.get(1), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[25] + "')]");
		postalCode.get(1).sendKeys(shipmentdata[26]);
		CommonFunctions.scrolltoElement(pickUpNumber.get(1));
		pickUpNumber.get(1).sendKeys(shipmentdata[27]);
		CommonFunctions.scrolltoElement(facilityOperatingFromTime.get(1));
		facilityOperatingFromTime.get(1).sendKeys(shipmentdata[28]);
		facilityOperatingToTime.get(1).sendKeys(shipmentdata[29]);
		CommonFunctions.scrolltoElement(contactNumber.get(1));
		contactNumber.get(1).sendKeys(shipmentdata[30]);
		CommonFunctions.scrolltoElement(instruction.get(1));
		instruction.get(1).sendKeys(shipmentdata[31]);
		CommonFunctions.scrolltoElement(pickDropDateList.get(1));
		pickDropDateList.get(1).click();
		pickdate.get(1).sendKeys(shipmentdata[32]);
		CommonFunctions.scrolltoElement(nameOfTheCommodity.get(0));
		nameOfTheCommodity.get(0).click();
		CommonFunctions.waitandNormalClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.waitandClick(addButton.get(1), 30);
		//		if (CommonFunctions.waitForVisiblity(addedcommodity.get(2), 30)) {
		//			assertEquals(true, true);
		//		} else {
		//			assertEquals(false, true);
		//		}

	}

	public void validate_success_msg() throws Throwable {
		CommonFunctions.waitandClick(continueButton, 30);
		if (shipmentSuccessMsg.isDisplayed()) {
			CommonFunctions.waitandClick(successClose, 30);
			softassert.assertEquals(true, true);
			System.out.println("New Shipment created successfully...");
		} else {
			softassert.assertEquals(false, true);
			System.out.println("New Shipment creation is unsuccessfull...");
		}

	}

	public void logout() throws Throwable {
		ManageShipmentsPage.assignCarrier.click();
		ManageShipmentsPage.clickOnAssignCarrier.sendKeys(Configprops.carriername);
		Thread.sleep(3000);
		ManageShipmentsPage.clickOnAssignCarrier.sendKeys(Keys.TAB);
		CommonFunctions.waitandClick(ManageShipmentsPage.AssignCarrierButton, 20);
		CommonFunctions.waitForVisiblity(AssignedCarrierSuccessMsg, 10);
		if(AssignedCarrierSuccessMsg.isDisplayed())
			CommonFunctions.waitandClick(successClose, 30);
		WebLoginPage.logout();
	}

	public void login_as_CarrierAccount() throws Throwable {
		WebLoginPage.logInCarrier();
	}

	public void accept_the_shipment() throws Throwable {
		CommonFunctions.waitandNormalClick(ManageShipmentsPage.notification, 30);
		CommonFunctions.waitandClick(ManageShipmentsPage.ClickOnShipment.get(0), 30);
		webdriver.navigate().refresh();
		CommonFunctions.waitUntilPageLoads();
		CommonFunctions.waitandClick(ManageShipmentsPage.AcceptButton, 30);
		if (CommonFunctions.waitForVisiblity(ManageShipmentsPage.SuccessMessage, 30)) {
			CommonFunctions.waitandClick(successClose, 30);
			assertEquals(true, true);
			System.out.println("Shippment Accepted");
		}

		else
			assertEquals(false, true);
	}

	public void add_driver_to_the_shipment() throws Throwable {
		CommonFunctions.clickUsingJavaExecutor(ManageShipmentsPage.SelectDriver);
		CommonFunctions.clickUsingJavaExecutor(DriverName);
		CommonFunctions.waitandClick(Assign_button, 10);
		System.out.println("Driver assignned");

	}

	public void dispatch() throws Exception {
		CommonFunctions.waitForVisiblity(dispatch_button, 10);
		CommonFunctions.waitandClick(dispatch_button, 10);
		CommonFunctions.waitandClick(successClose, 30);
		System.out.println("Dispatched Successfully");
	}

	public void filter_shipment() {
		CommonFunctions.scrolltoElement(manageShipments);
		manageShipments.click();
		shipmentId_filter.click();
		shipmentId_filter.sendKeys(shipmentID);
	}

	public void verify_exceptionflag() {
		assertTrue(exception_flag.isDisplayed());
	}


	public void add_pick_createShipment() throws Throwable {

		address.get(0).sendKeys(shipmentdata[9]);
		companyName.get(0).sendKeys(shipmentdata[8]);
		country.get(0).click();
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[10] + "')]");
		Thread.sleep(10);
		city.get(0).sendKeys(shipmentdata[11]);
		CommonFunctions.waitandNormalClick(state.get(0), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[12] + "')]");
		CommonFunctions.scrolltoElement(postalCode.get(0));
		postalCode.get(0).sendKeys(shipmentdata[13]);
		CommonFunctions.scrolltoElement(pickUpNumber.get(0));
		pickUpNumber.get(0).sendKeys(shipmentdata[14]);
		CommonFunctions.scrolltoElement(facilityOperatingFromTime.get(0));
		facilityOperatingFromTime.get(0).sendKeys(shipmentdata[15]);
		facilityOperatingToTime.get(0).sendKeys(shipmentdata[16]);
		CommonFunctions.scrolltoElement(contactNumber.get(0));
		contactNumber.get(0).sendKeys(shipmentdata[17]);
		CommonFunctions.scrolltoElement(instruction.get(0));
		instruction.get(0).sendKeys(shipmentdata[18]);
		CommonFunctions.scrolltoElement(pickdate.get(0));
		pickdate.get(0).clear();
		pickdate.get(0).sendKeys(CommonFunctions.getDate(0, "MM/dd/yyyy"));
		CommonFunctions.scrolltoElement(appointmentNedded.get(0));
		CommonFunctions.waitandClick(appointmentNedded.get(0), 30);
		CommonFunctions.scrolltoElement(appointmentTime.get(0));
		appointmentTime.get(0).sendKeys(shipmentdata[20]);

		CommonFunctions.scrolltoElement(nameOfThePick1Commodity.get(0));
		CommonFunctions.waitandNormalClick(nameOfThePick1Commodity.get(0), 5);
		CommonFunctions.waitandClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.scrolltoElement(pickCommodityQtyInput.get(0));
		pickCommodityQtyInput.get(0).clear();
		pickCommodityQtyInput.get(0).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.scrolltoElement(nameOfThePick1Commodity.get(1));
		CommonFunctions.waitandNormalClick(nameOfThePick1Commodity.get(1), 5);
		CommonFunctions.waitandClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.scrolltoElement(pickCommodityQtyInput.get(1));
		pickCommodityQtyInput.get(1).clear();
		pickCommodityQtyInput.get(1).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.scrolltoElement(pickBOLUpload.get(0));
		pickBOLUpload.get(0).sendKeys(workingDir + "\\BOL Sample\\1.PNG");
		CommonFunctions.waitForVisiblity(uploadCompleteMsg, 30);
		CommonFunctions.clickUsingJavaExecutor(uploadClose);
		CommonFunctions.scrolltoElement(pickSaveBtn.get(0));
		CommonFunctions.waitandClick(pickSaveBtn.get(0), 30);
		if (CommonFunctions.waitForVisiblity(P1, 30)) {
			assertEquals(true, true);
		} else {
			assertEquals(false, true);
		}

	}


	public void add_pick2_createShipment() throws Throwable {
		CommonFunctions.waitandClick(addAnotherPick, 30);
		address.get(1).sendKeys(shipmentdata1[9]);
		companyName.get(1).sendKeys(shipmentdata1[8]);
		CommonFunctions.waitandNormalClick(country.get(1), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata1[10] + "')]");
		Thread.sleep(10);
		city.get(1).sendKeys(shipmentdata1[11]);
		CommonFunctions.waitandNormalClick(state.get(1), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata1[12] + "')]");
		CommonFunctions.scrolltoElement(postalCode.get(1));
		postalCode.get(1).sendKeys(shipmentdata1[13]);
		CommonFunctions.scrolltoElement(pickUpNumber.get(1));
		pickUpNumber.get(1).sendKeys(shipmentdata[14]);
		CommonFunctions.scrolltoElement(facilityOperatingFromTime.get(1));
		facilityOperatingFromTime.get(1).sendKeys(shipmentdata[15]);
		facilityOperatingToTime.get(1).sendKeys(shipmentdata[16]);
		CommonFunctions.scrolltoElement(contactNumber.get(1));
		contactNumber.get(1).sendKeys(shipmentdata[17]);
		CommonFunctions.scrolltoElement(instruction.get(1));
		instruction.get(1).sendKeys(shipmentdata[18]);
		CommonFunctions.scrolltoElement(pickdate.get(1));
		pickdate.get(1).clear();
		pickdate.get(1).sendKeys(CommonFunctions.getDate(0, "MM/dd/yyyy"));
		CommonFunctions.scrolltoElement(nameOfThePick2Commodity.get(0));
		CommonFunctions.waitandClick(nameOfThePick2Commodity.get(0), 5);
		CommonFunctions.waitandClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.scrolltoElement(pickCommodityQtyInput.get(2));
		pickCommodityQtyInput.get(2).clear();
		pickCommodityQtyInput.get(2).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.scrolltoElement(nameOfThePick2Commodity.get(1));
		CommonFunctions.waitandNormalClick(nameOfThePick2Commodity.get(1), 5);
		CommonFunctions.waitandClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.scrolltoElement(pickCommodityQtyInput.get(3));
		pickCommodityQtyInput.get(3).clear();
		pickCommodityQtyInput.get(3).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.scrolltoElement(pickBOLUpload.get(1));
		pickBOLUpload.get(1).sendKeys(workingDir + "\\BOL Sample\\2.PNG");
		CommonFunctions.waitForVisiblity(uploadCompleteMsg, 30);
		CommonFunctions.clickUsingJavaExecutor(uploadClose);
		CommonFunctions.scrolltoElement(pickSaveBtn.get(1));
		CommonFunctions.waitandNormalClick(pickSaveBtn.get(1), 30);
		if (CommonFunctions.waitForVisiblity(P1, 30)) {
			assertEquals(true, true);
		} else {
			assertEquals(false, true);
		}

	}


	public void add_drop_createShipment() throws Throwable {
		address.get(2).sendKeys(shipmentdata[22]);
		companyName.get(2).sendKeys(shipmentdata[21]);
		country.get(2).click();
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[23] + "')]");
		Thread.sleep(10);
		city.get(2).sendKeys(shipmentdata[24]);
		CommonFunctions.waitandNormalClick(state.get(2), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata[25] + "')]");
		CommonFunctions.scrolltoElement(postalCode.get(2));
		postalCode.get(2).sendKeys(shipmentdata[26]);
		CommonFunctions.scrolltoElement(pickUpNumber.get(2));
		pickUpNumber.get(2).sendKeys(shipmentdata[27]);
		CommonFunctions.scrolltoElement(facilityOperatingFromTime.get(2));
		facilityOperatingFromTime.get(2).sendKeys(shipmentdata[28]);
		facilityOperatingToTime.get(2).sendKeys(shipmentdata[29]);
		CommonFunctions.scrolltoElement(contactNumber.get(2));
		contactNumber.get(2).sendKeys(shipmentdata[30]);
		CommonFunctions.scrolltoElement(instruction.get(2));
		instruction.get(2).sendKeys(shipmentdata[31]);
		CommonFunctions.scrolltoElement(pickdate.get(2));
		pickdate.get(2).clear();
		pickdate.get(2).sendKeys(CommonFunctions.getDate(0, "MM/dd/yyyy"));
		CommonFunctions.scrolltoElement(nameOfTheDropCommodity.get(0));
		CommonFunctions.waitandNormalClick(nameOfTheDropCommodity.get(0), 5);
		CommonFunctions.waitandNormalClick(nameOfTheCommodityList.get(0), 5);
		CommonFunctions.scrolltoElement(dropCommodityQtyInput.get(0));
		dropCommodityQtyInput.get(0).clear();
		dropCommodityQtyInput.get(0).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.scrolltoElement(nameOfTheDropCommodity.get(1));
		CommonFunctions.waitandNormalClick(nameOfTheDropCommodity.get(1), 5);
		CommonFunctions.waitandNormalClick(nameOfTheCommodityList.get(0), 5);
		CommonFunctions.scrolltoElement(dropCommodityQtyInput.get(1));
		dropCommodityQtyInput.get(1).clear();
		dropCommodityQtyInput.get(1).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.waitandNormalClick(dropBOL, 5);
		Actions action = new Actions(ObjectHelper.webdriver);
		WebElement we = ObjectHelper.webdriver.findElement(
				By.xpath("//ul[@role='listbox']/li[div[span[text()='1.PNG']]]//span/span/input[@type='checkbox']"));
		action.click(we).build().perform();
		action.moveToElement(we).build().perform();
		action.moveByOffset(-100, -100).build().perform();
		action.click().build().perform();
		CommonFunctions.scrolltoElement(dropSaveBtn.get(0));
		CommonFunctions.waitandNormalClick(dropSaveBtn.get(0), 30);
		if (CommonFunctions.waitForVisiblity(D1, 30)) {
			assertEquals(true, true);
		} else {
			assertEquals(false, true);
		}

	}


	public void add_drop2_createShipment() throws Throwable {
		CommonFunctions.waitandClick(addAnotherDrop, 30);
		address.get(3).sendKeys(shipmentdata1[22]);
		companyName.get(3).sendKeys(shipmentdata1[21]);
		CommonFunctions.waitandNormalClick(country.get(3), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata1[23] + "')]");
		Thread.sleep(10);
		city.get(3).sendKeys(shipmentdata1[24]);
		CommonFunctions.waitandNormalClick(state.get(3), 5);
		CommonFunctions.clickUsingJavaExecutor("//ul//li[contains(.,'" + shipmentdata1[25] + "')]");
		CommonFunctions.scrolltoElement(postalCode.get(3));
		postalCode.get(3).sendKeys(shipmentdata1[26]);
		CommonFunctions.scrolltoElement(pickUpNumber.get(3));
		pickUpNumber.get(3).sendKeys(shipmentdata[27]);
		CommonFunctions.scrolltoElement(facilityOperatingFromTime.get(3));
		facilityOperatingFromTime.get(3).sendKeys(shipmentdata[28]);
		facilityOperatingToTime.get(3).sendKeys(shipmentdata[29]);
		CommonFunctions.scrolltoElement(contactNumber.get(3));
		contactNumber.get(3).sendKeys(shipmentdata[30]);
		CommonFunctions.scrolltoElement(instruction.get(3));
		instruction.get(3).sendKeys(shipmentdata[31]);
		CommonFunctions.scrolltoElement(pickdate.get(3));
		pickdate.get(3).clear();
		pickdate.get(3).sendKeys(CommonFunctions.getDate(0, "MM/dd/yyyy"));
		CommonFunctions.scrolltoElement(nameOfTheDropCommodity.get(2));
		CommonFunctions.waitandNormalClick(nameOfTheDropCommodity.get(2), 5);
		CommonFunctions.waitandClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.scrolltoElement(dropCommodityQtyInput.get(2));
		dropCommodityQtyInput.get(2).clear();
		dropCommodityQtyInput.get(2).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.scrolltoElement(nameOfTheDropCommodity.get(3));
		CommonFunctions.waitandNormalClick(nameOfTheDropCommodity.get(3), 5);
		CommonFunctions.waitandClick(nameOfTheCommodityList.get(0), 30);
		CommonFunctions.scrolltoElement(dropCommodityQtyInput.get(3));
		dropCommodityQtyInput.get(3).clear();
		dropCommodityQtyInput.get(3).sendKeys(Integer.parseInt(shipmentdata[4]) / 2 + "");

		CommonFunctions.waitandNormalClick(dropBOL, 5);
		Actions action = new Actions(ObjectHelper.webdriver);
		WebElement we = ObjectHelper.webdriver.findElement(
				By.xpath("//ul[@role='listbox']/li[div[span[text()='2.PNG']]]//span/span/input[@type='checkbox']"));
		action.click(we).build().perform();
		action.moveToElement(we).build().perform();
		action.moveByOffset(-100, -100).build().perform();
		action.click().build().perform();
		CommonFunctions.scrolltoElement(dropSaveBtn.get(1));
		CommonFunctions.waitandClick(dropSaveBtn.get(1), 30);
		if (CommonFunctions.waitForVisiblity(D1, 30)) {
			assertEquals(true, true);
		} else {
			assertEquals(false, true);
		}

	}


	public void assign_shipper_carrier() {
		try {
			ManageShipmentsPage.assignCarrier.click();
			ManageShipmentsPage.clickOnAssignCarrier.sendKeys(Configprops.carriername);
			Thread.sleep(3000);
			ManageShipmentsPage.clickOnAssignCarrier.sendKeys(Keys.TAB);
			CommonFunctions.waitandClick(ManageShipmentsPage.AssignCarrierButton, 20);
			CommonFunctions.waitForVisiblity(AssignedShipperCarrierSuccessMsg, 10);
			if(AssignedShipperCarrierSuccessMsg.isDisplayed())
				CommonFunctions.waitandClick(successClose, 30);
			webdriver.navigate().refresh();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void disable_commmodityImages() 
	{
		try {
			CommonFunctions.waitandClick(edit_shippingdetails, 10);
			CommonFunctions.clickUsingJavaExecutor(commodityImages_toggleOFF);
			CommonFunctions.waitandClick(save_shippingdetails, 10);
			CommonFunctions.waitandClick(successClose, 30);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void enable_RTO() {
		try {
			CommonFunctions.waitandClick(edit_shippingdetails, 10);
			CommonFunctions.clickUsingJavaExecutor(RTO_toggleOFF);
			CommonFunctions.waitandClick(save_shippingdetails, 10);
			CommonFunctions.waitandClick(successClose, 30);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void verify_pickdrop_colouring(String expectedcolor) throws Exception {
		String actualColor = CommonFunctions.get_color(pick_color);	
		if(actualColor.equalsIgnoreCase(expectedcolor))
			System.out.println("Pick/drop icon coloring is as expected");
		else
			System.out.println("Pick/drop icon coloring is not as expected");

	}

	public void verify_RTO_colouring(String expectedcolor) throws Exception {
		String actualColor = CommonFunctions.get_color(RTO_color);	

		if(actualColor.equalsIgnoreCase(expectedcolor))
			System.out.println("RTO icon coloring is as expected");
		else
			System.out.println("RTO icon coloring is not as expected");

	}

	public void update_pickdrop_name(String pickname, String dropname) {
		try {
			CommonFunctions.waitandClick(edit_pickdropdetails, 10);
			if (expand_all.isDisplayed())
				expand_all.click();
			companyName.get(0).clear();
			companyName.get(0).sendKeys(pickname);
			CommonFunctions.scrolltoElement(companyName.get(1));
			companyName.get(1).clear();
			companyName.get(1).sendKeys(dropname);
			CommonFunctions.waitandClick(save_pickdropdetails, 10);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
