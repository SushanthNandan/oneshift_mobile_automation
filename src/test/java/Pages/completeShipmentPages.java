package Pages;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.itextpdf.text.log.SysoCounter;

import Utilities.BaseClass;
import Utilities.CommonFunctions;
import Utilities.CommonLib;
import Utilities.XLHandler;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.*;


public class completeShipmentPages {

	static AndroidDriver<MobileElement> androiddriver = BaseClass.getDriver();
	SoftAssert softAssert = new SoftAssert();
	String[] shipmentdata = XLHandler.readexcel("19_S6_2886_1", 1, "NewTestData.xlsx");
	String[] shipmentdata1 = XLHandler.readexcel("19_S6_2886_1", 3, "NewTestData.xlsx");

	@AndroidFindBy(xpath="//*[contains(@content-desc,'ItemCard')]")
	static List<MobileElement> total_pickDrop;
	@AndroidFindBy(xpath="//android.widget.TextView[@content-desc='ArrowLeft-Button']")
	static MobileElement arrow_left_button;
	//  Selectpick	
	@AndroidFindBy(xpath="//*[contains(@content-desc,'ItemCard')]")
	static MobileElement first_pickDrop;
	@AndroidFindBy(xpath="//*[@content-desc='MainHeader-OnDutyChanged']")
	static MobileElement toggle;
	@AndroidFindBy(xpath="//*[@text='GO OFF DUTY']")
	static MobileElement goOff_duty;
	@AndroidFindBy(xpath="//*[@text='ENABLE NOW']")
	static MobileElement enable_now;
	//	Checked in screen
	@AndroidFindBy(xpath="//*[@content-desc='ItemScreen-Content-Text']")
	static MobileElement checkin_title;
	@AndroidFindBy(xpath="//*[@content-desc='ItemScreen-Content-ConfirmButton']")
	static MobileElement checkin_confirm;
	@AndroidFindBy(xpath="//*[@content-desc='ItemScreen-Content-ReportButton']")
	static MobileElement checkin_rap;
	@AndroidFindBy(xpath="//*[@content-desc='ItemScreen-Content-ItemCard-scheduledAt']")
	static MobileElement scheduled_date;
	@AndroidFindBy(xpath="//*[@content-desc='ItemScreen-Content-ItemCard-appointmentAt']")
	static MobileElement appointment_time;
	@AndroidFindBy(xpath="//*[@content-desc='ItemScreen-Content-ItemCard-addressLine1']")
	static MobileElement address_line1;
	@AndroidFindBy(xpath="//*[@content-desc='ItemScreen-Content-ItemCard-city']")
	static MobileElement address_city;


	//	Finished loading
	@AndroidFindBy(xpath="//*[@content-desc='FinishScreen-Content-TimerTitle']")
	static MobileElement time_at_facility;
	@AndroidFindBy(xpath="//*[@content-desc='FinishScreen-Content-ActionTitle']")
	static MobileElement finishedloading_title;
	@AndroidFindBy(xpath="//android.widget.TextView[@content-desc='FinishScreen-Content-ConfirmButton-Text']")
	static MobileElement finishedloading_confirm;
	@AndroidFindBy(xpath="//*[@content-desc='FinishScreen-Content-ReportButton']")
	static MobileElement finish_rap;
	@AndroidFindBy(xpath="//android.widget.TextView[@content-desc='FinishScreen-Content-ConfirmButton-Text']")
	static MobileElement Upload_POD;
	@AndroidFindBy(xpath="//android.view.ViewGroup[@content-desc='PhotoGrid-UploadButton']")
	static MobileElement Upload_POD_Image;
	@AndroidFindBy(xpath="//android.widget.TextView[@content-desc='CameraScreen-Footer-Camera-Button-Icon']")
	static MobileElement click_pic;
	@AndroidFindBy(xpath="//android.widget.TextView[@content-desc='CameraScreen-UsePhotoButton-Text']")
	static MobileElement Use_Photo;
	//	Pick Complete
	//	@AndroidFindBy(xpath="//*[@content-desc='GradientButton-onBtnPress']")
	@AndroidFindBy(xpath="//*[@content-desc='CompleteActionScreen-NextButton']")
	static MobileElement pick_next;
	@AndroidFindBy(xpath="//*[contains(@text='COMPLETE')]")
	static MobileElement complete_title;
	//	Upload POD
	@AndroidFindBy(xpath="//*[@content-desc='PhotoGrid-UploadButton' or @content-desc='PhotoGrid-CameraIcon']")
	static MobileElement select_image;
	@AndroidFindBy(xpath="//*[@content-desc='CameraScreen-Footer-Camera-Button-Inner']")
	static MobileElement camera;
	@AndroidFindBy(id = "com.oppo.camera:id/shutter_button")
	static MobileElement camera_icon;
	@AndroidFindBy(id="com.oppo.camera:id/done_button")
	static MobileElement camera_imageOK;
	@AndroidFindBy(xpath="//*[@content-desc='CameraScreen-Close-Icon']")
	static MobileElement camera_close;
	@AndroidFindBy(xpath="//*[@content-desc='CameraScreen-Footer-Retake-Button']")
	static MobileElement retake;
	@AndroidFindBy(xpath="//*[@content-desc='PhotoGrid-onPhotoSelect']")
	static MobileElement close_photo;
	@AndroidFindBy(xpath="//*[@content-desc='UploadPhotoScreen-onUploadPhotosLater']")
	static MobileElement upload_later;
	@AndroidFindBy(xpath="//*[@class='android.widget.ProgressBar']")
	static MobileElement progress_bar;
	//rap screen
	@AndroidFindBy(xpath="//*[@text='Choose an option']")
	static MobileElement choose_option;
	@AndroidFindBy(xpath="//*[@text='Will be running late']")
	static MobileElement running_late;
	@AndroidFindBy(xpath="//*[@text='Will not make it']")
	static MobileElement willnot_makeit;
	@AndroidFindBy(xpath="//*[@text='No impact']")
	static MobileElement no_impact;
	@AndroidFindBy(xpath="//*[@text='Product Issue']")
	static MobileElement product_issue;
	@AndroidFindBy(xpath="//*[contains(@text,'Receiver Issue')]")
	static MobileElement reciever_issue;
	@AndroidFindBy(xpath="//*[contains(@text,'Driver Issue')]")
	static MobileElement driver_issue;
	@AndroidFindBy(xpath="//*[contains(@text,'weather')]")
	static MobileElement weather;
	@AndroidFindBy(xpath="//*[@text='Customs Issue']")
	static MobileElement custom_issue;
	@AndroidFindBy(xpath="//*[@text='Other']")
	static MobileElement other;
	@AndroidFindBy(xpath="//*[@content-desc='ReportProblemScreen-onChangeText']")
	static MobileElement textfiled;	 
	@AndroidFindBy(xpath="//*[@content-desc='ReportProblemScreen-NextButton']")
	static MobileElement rap_next;
	@AndroidFindBy(xpath="//*[@content-desc='UploadPhotoScreen-UploadPhotosButton']")
	static MobileElement upload;
	@AndroidFindBy(xpath="//*[@content-desc='PhotoPreviewScreen-CloseIcon']")
	static MobileElement cancelPhoto;
	@AndroidFindBy(xpath="//*[@content-desc='PhotoPreviewScreen-UsePhotoButtonText']")
	static MobileElement usePhoto;
	@AndroidFindBy(xpath="//*[@content-desc='CompleteScreen-OkButton']")
	static MobileElement ok;

	/*---Shipment details section----*/
	@AndroidFindBy(xpath="//android.view.ViewGroup[@content-desc='DetailsButton-Button']/android.view.ViewGroup")
	static MobileElement shipmentdetails;
	@AndroidFindBy(xpath="//*[@text='SHIPMENT DETAILS']")
	static MobileElement shipmentdetails_title;
	@AndroidFindBy(xpath="//*[@text='Require Commodity Images']")
	static MobileElement commodity_image_heading;
	@AndroidFindBy(xpath="//*[@content-desc='DetailsContainer-CloseIcon']")
	static MobileElement shipmentdetails_close;
	
	@AndroidFindBy(xpath = "//*[@content-desc='PopupBase-closeCallback']")
	static MobileElement popup_close;
	/*----Commodity Images screen ---*/
	@AndroidFindBy(xpath="//*[@content-desc='UploadPhotoScreen-Title-Text']")
	static MobileElement Commodity_images_title;
	@AndroidFindBy(xpath="//*[@text='Your dispatch requires you to take pictures of the product.']")
	static MobileElement commodity_images_description;

	/*-------Sync screen and POPUP notifications-----*/
	@AndroidFindBy(xpath="//*[@text='Changes are being made']")
	static MobileElement sync_description;

	@AndroidFindBy(xpath="//*[@text='THE FOLLOWING CHANGES HAVE BEEN MADE TO YOUR ACTIVE SHIPMENT']")
	static MobileElement Popup_notificationtitle;	

	@AndroidFindBy(xpath="//*[@text='VIEW IN DETAILS']")
	static MobileElement viewdetails_button;	

	@AndroidFindBy(xpath="//*[@content-desc='PopupBase-closeCallback']")
	static MobileElement close_popup;

	public completeShipmentPages(AppiumDriver<MobileElement> androiddriver) {
		PageFactory.initElements(new AppiumFieldDecorator(androiddriver), this);
		LoginPage.androiddriver = androiddriver;
	}


	public void verify_title(String exptitle) {
		String acttitle = checkin_title.getText();
		boolean is_true = acttitle.contains(exptitle);
		Assert.assertTrue("Sign in title is not verified",is_true);

	}


	public MobileElement find(String ele) {
		return androiddriver.findElement(By.xpath(ele));
	}

	public void select_availablePickDrop() throws InterruptedException {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, first_pickDrop, 20);
		first_pickDrop.click();
		//		if(!(progress_bar.isDisplayed())) {
		//			System.out.println("first pick not clicked");
		//			first_pickDrop.click();
	}

	public void verify_checkin_text(String exptext) {
		CommonLib.waitforelement(androiddriver, checkin_title, 20);
		String actText = checkin_title.getText();
		assertTrue(actText.contains(exptext));			
	}

	public void click_confirm_checkin() {
		CommonLib.waitforelement(androiddriver, checkin_confirm, 20);
		checkin_confirm.click();

	}

	public void verify_finishloading_title(String exptext) {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, finishedloading_title, 20);
		softAssert.assertTrue( finishedloading_title.getText().contains(exptext), "Driver is not in Finished load/unload screen");

	}

	public void click_finish_checkin() throws InterruptedException {
		CommonLib.waitforelement(androiddriver, finishedloading_confirm, 20);
		finishedloading_confirm.click();
		Thread.sleep(5000);

	}

	public void verify_Complete_Title(String exptitle) {
		androiddriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, complete_title, 30);		
		softAssert.assertTrue(complete_title.getText().contains(exptitle), "Complete screen is not displayed");		

	}

	public void select_PickDrop_offduty() throws InterruptedException {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//		first_pickDrop.click();
		softAssert.assertTrue(enable_now.isDisplayed());
	}

	public void Click_next_button() {
		androiddriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, pick_next, 10);
		pick_next.click();

	}

	public void click_upload_POD() {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, finishedloading_confirm, 10);
		finishedloading_confirm.click();		
	}

	public void Select_upload_image() {
		androiddriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, select_image, 10);
		select_image.click();		
	}

	public void allow_permissions() {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			homePAgeObjects.allow.click();				
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}

	public void Camera_photo() {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, camera_icon, 20);
		camera_icon.click();
		CommonLib.waitforelement(androiddriver, camera_imageOK, 20);
		camera_imageOK.click();

	}


	public void Cancel_Captured_photo() {
		androiddriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, cancelPhoto, 20);
		cancelPhoto.click();			
	}

	public void Select_Captured_photo() {
		androiddriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, usePhoto, 20);
		usePhoto.click();			
	}

	public void Upload_selected_POD() throws InterruptedException {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, upload, 20);
		upload.click();			
		Thread.sleep(9000);
	}

	//rap methods
	public void rap_checkin() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		checkin_rap.click();
	}

	public void choose_option() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		choose_option.click();
	}

	public void no_impact() {
		no_impact.click();
	}

	public void running_late() {
		running_late.click();
	}

	public void willnot_make() {
		willnot_makeit.click();
	}

	public void next_rap() {
		rap_next.click();
	}

	public void Report() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		upload.click();
	}

	public void usephoto_rap() {
		androiddriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		usePhoto.click();
	}

	public void uploadLater() {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		upload_later.click();
	}

	public void rap_loading() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		finish_rap.click();
	}

	public void go_offDuty() {
		toggle.click();
		goOff_duty.click();		
	}

	public void enable_now() {
		enable_now.click();
	}

	public void Complete_shipment() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, ok, 30);
		verify_Complete_Title("COMPLETE");
		ok.click();
		softAssert.assertAll();
	}


	public void select_issue(String option) {

		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		switch(option) {
		case "Product Issue":
			product_issue.click();
			break;
		case "Receiver Issue":
			reciever_issue.click();
			break;
		case "Driver Issue":
			driver_issue.click();
			break;
		case "weather":
			weather.click();
			break;
		case "Custom Issue":
			custom_issue.click();
			break;
		case "Other":
			other.click();
			textfiled.sendKeys("Other issue");
			break;
		}
		System.out.println("Option is.."+option);

	}

	public void Verify_scheduled_date() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		String date = scheduled_date.getText();
		System.out.println("date....");
		System.out.println(date);
		System.out.println(CommonFunctions.getDate(0, "MM/dd/yyyy"));

	}

	public void Verify_addressline() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		String addressline = address_line1.getText();
		System.out.println("address line....");
		System.out.println(addressline);
		System.out.println(shipmentdata[9]);

	}


	public void Verify_addresscity() {
		androiddriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		String addresscity = address_city.getText();
		System.out.println("city is....");
		System.out.println(addresscity);
		System.out.println(shipmentdata[11]);

	}

	public void shipmentdetails_screen(String title) {
		CommonLib.waitforelement(androiddriver, shipmentdetails, 10);
		shipmentdetails.click();
		softAssert.assertEquals(shipmentdetails_title.getText().equalsIgnoreCase(title), true, "Shipment details screen is not displayed");
	}
	public void verify_commmodity_images_text(String title) {
		CommonLib.waitforelement(androiddriver, commodity_image_heading, 10);
		String actText = commodity_image_heading.getText();
		softAssert.assertTrue( actText.contains(title), "Require commodity images heading is not displayed");

	}

	public void close_shipmentdetails() {
		CommonLib.waitforelement(androiddriver, shipmentdetails_close, 10);
		shipmentdetails_close.click();
	}

	public void Commodity_images_screen(String exptext) {
		androiddriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonLib.waitforelement(androiddriver, Commodity_images_title, 30);
		String actText = Commodity_images_title.getText();
		assertEquals(true, actText.contains(exptext),"Commodity_images_title is not displayed" );
		softAssert.assertTrue(commodity_images_description.isDisplayed(), "Commodity image desc is not present");
	}


	public void verify_pick_companyName() {
		androiddriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		String companyname = WebCreateShipment.pick_companyName;
		MobileElement pick_companyname = androiddriver.findElement(By.xpath("//*[@text='"+companyname+"']"));
		CommonLib.waitforelement(androiddriver, pick_companyname, 15);
		if(pick_companyname.isDisplayed())
			System.out.println("Pick Company name is displayed");
		else
			softAssert.assertEquals(true, false, "Pick Company name is not displayed");
	}

	public void verify_drop_companyName() {
		androiddriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		String companyname = WebCreateShipment.drop_companyName;
		MobileElement drop_companyname = androiddriver.findElement(By.xpath("//*[@text='"+companyname+"']"));
		CommonLib.waitforelement(androiddriver, drop_companyname, 15);
		if(drop_companyname.isDisplayed())
			System.out.println("Drop Company name is displayed");
		else
			softAssert.assertEquals(true, false, "Drop Company name is not displayed");
	}

	public void navigate_to_previous_screen()
	{
		CommonLib.waitforelement(androiddriver, arrow_left_button, 10);
		arrow_left_button.click();

	}

	public void verifySyncScreen() {
		CommonLib.waitforelement(androiddriver, sync_description, 10);
		softAssert.assertTrue(sync_description.isDisplayed(), "Sync screen is not displayed");
	}

	public void verify_popup_notification(String pickname, String dropname) {
		CommonLib.waitforelement(androiddriver,Popup_notificationtitle, 15);
		CommonLib.waitforelement(androiddriver, close_popup, 10);
		if(Popup_notificationtitle.isDisplayed() && viewdetails_button.isDisplayed()) {
			viewdetails_button.click();
		//	softAssert.assertEquals(shipmentdetails_title.isDisplayed(), true, "Shipment details screen is not displayed");
			MobileElement pick_companyname = androiddriver.findElement(By.xpath("//*[@text='"+pickname+"']"));
			MobileElement drop_companyname = androiddriver.findElement(By.xpath("//*[@text='"+dropname+"']"));
			softAssert.assertTrue(pick_companyname.isDisplayed(), "Pick company name is not being updated");
			softAssert.assertTrue(drop_companyname.isDisplayed(), "Drop company name is not being updated");
			shipmentdetails_close.click();
			System.out.println("Pop Up notification is displayed successfully");
		}
			
		else {
			softAssert.assertTrue(false, "Pop Up notification and details is not displayed successfully");
			System.out.println("Pop Up notification and details is not displayed successfully");
		}

	}
}
