package StepDefinition;

import Pages.ManageShipmentsPage;
import Pages.WebCreateShipment;
import Pages.WebLoginPage;
import Utilities.BaseClass;
import Utilities.ObjectHelper;
import cucumber.api.java.en.*;

public class WeCreateShipment_stepDef {
	static WebCreateShipment createshipment = new WebCreateShipment(BaseClass.getwebDriver());
	static WebLoginPage weblogin = new WebLoginPage(BaseClass.getwebDriver());
	static ManageShipmentsPage manageShipments = new ManageShipmentsPage(BaseClass.getwebDriver());



	@When("^I create shipment in web page$")
	public static void create_shipment() throws Throwable {
		createshipment.click_on_manage_shipments();
		createshipment.click_on_create_new_shipment();
		createshipment.click_on_next();
		createshipment.add_commodities_section();
		createshipment.add_pick();
		createshipment.add_drop();
		createshipment.validate_success_msg();		
		createshipment.logout();
	}

	@When("^I create duplicate shipment in web page$")
	public static void create_duplciate_shipment() throws Throwable {
		createshipment.click_on_manage_shipments();
		createshipment.duplicate_shipments();
		createshipment.validate_success_msg();
	}

	@When("^I disable Capture Commodity Images$")
	public static void commodity_images_toggleoff() {
		createshipment.disable_commmodityImages();
	}
	
	@And("^I enable Return to Origin$")
	public static void enable_return_to_origin() {
		createshipment.enable_RTO();
	}

	@When("^I create shipment in web page with BOL$")
	public static void create_BOL_shipment() throws Throwable {
		createshipment.click_on_manage_shipments();
		createshipment.click_on_create_new_shipment();
		createshipment.click_on_next();
		createshipment.add_commodities_section();
		//		createshipment.add_another_commodity();
		createshipment.add_pick_createShipment();
		createshipment.add_pick2_createShipment();
		createshipment.add_drop_createShipment();
		createshipment.add_drop2_createShipment();
		createshipment.validate_success_msg();		
		createshipment.logout();


	}

	@And("^I accept assign the shipment$")
	public void accept_assign_shipment() throws Throwable {
		createshipment.login_as_CarrierAccount();
		createshipment.accept_the_shipment();
		createshipment.add_driver_to_the_shipment();
		createshipment.dispatch();

	}

	@And("^I relogin web as carrier$")
	public void login_carrier() throws Throwable {
		weblogin.logout();
		createshipment.login_as_CarrierAccount();

	}

	@And("^I assign carrier and dispatch$")
	public void assign_carrier_and_dispatch() throws Throwable {
		createshipment.assign_shipper_carrier();
		createshipment.add_driver_to_the_shipment();
		createshipment.dispatch();

	}




	@Then("^I get dispatch number from company info$")
	public void getweb_dispatchnum () {
		weblogin.companyinfo();
		weblogin.getdispatch_num();
	}

	@When("^I filter the shipmentid in manage shipments$")
	public void filter_shipmentid() {
		createshipment.filter_shipment();
	}

	@Then("^I verify exception flag in manageshipments page$")
	public void exception_flag() {
		createshipment.verify_exceptionflag();
	}

	@Then("^I verify the colouring of pickdrop \"(.*)\" displayed$")
	public void pick_drop_colouring(String color) throws Exception {
		createshipment.verify_pickdrop_colouring(color);

	}
	
	@Then("^I verify the colouring of RTO \"(.*)\" displayed$")
	public void RTO_colouring(String color) throws Exception {
		createshipment.verify_RTO_colouring(color);

	}
	
	@When("^I update pick name \"(.*)\" and drop name \"(.*)\" in web$")
	public void update_pickdrop_name(String Pname, String Dname) throws Exception {
		createshipment.update_pickdrop_name(Pname, Dname);
	}
	


}
