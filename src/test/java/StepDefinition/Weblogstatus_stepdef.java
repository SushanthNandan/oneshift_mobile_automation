package StepDefinition;

import Pages.WebCreateShipment;
import Utilities.BaseClass;
import cucumber.api.java.en.*;
import Pages.webLogPages;

public class Weblogstatus_stepdef {
	static webLogPages weblogpages = new webLogPages(BaseClass.getwebDriver());
	
	@Then("^I verify Dispatched status in logs$")
	public void dispatched() {
		weblogpages.verify_dispatched();
		System.out.println("Dispatch status updated");
		
	}
	
	@Then("^I verify In Transit status in logs$")
	public void in_tranist() {
		weblogpages.verify_intransit();
	}

	@Then("^I verify checked in status in logs$")
	public void checkedin() {
		weblogpages.verify_checkedin();
	}
	
	@Then("^I verify finished loading status in logs$")
	public void finished() {
		weblogpages.verify_finished();
	}
	
	
	@Then("^I verify driver started status in logs$")
	public void driver_started() {
		weblogpages.verify_driverstarted();
	}
	
	@Then("^I verify exception status of \"(.*)\" in logs$")
	public void exception_status(String issue_type) {
		weblogpages.verify_exception();
		weblogpages.verify_exceptiontype(issue_type);;
				
	}
	
	@Then("^I verify POD added status not added in logs$")
	public void noPODadded() {
		weblogpages.verify_NoPOD();
	}
	
	@And("^I verify Commodity images status (\\d+) in logs$")
	public void commodity_images_status(int image) {
		weblogpages.verify_commodityImages_log(image);

	}
	
	@Then("^I verify off duty status in logs$")
	public void verify_offduty() {
		weblogpages.verify_offdutystatus();
	}
	
	@Then("^I verify on duty status in logs$")
	public void verify_onduty() {
		weblogpages.verify_ondutystatus();
	}
	
	@Then("^I verify POD added status in logs$")
	public void verify_PODAdded() {
		weblogpages.verify_POD();
	}
	
	@Then("^I verify shipment completed status in logs$")
	public void verify_completedstatus() {
		weblogpages.verify_completed();
	}
	
	@Then("^I verify total exceptions in overall status$")
	public void total_Excptions_status() {
		weblogpages.verify_totalExceptions();
	}
	
	@Then("^I verify late exception flag in status$")
	public void late_flag() {
		weblogpages.verify_lateflag();
	}

	@Then("^I verify running late exception flag in status$")
	public void runninglate_flag() {
		weblogpages.verify_running_lateflag();
	}
	
	
}
