package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
//
//import net.lightbody.bmp.BrowserMobProxy;

public class ObjectHelper {
	public static WebDriver webdriver;
	public static WebDriverWait wait;
	public static String browsertype = null;

	public static String screenshotsFolder;
	public static String downloadsFolder;
	public static String reportsFolder;

	/******* Email Report Info *******/
	public static String testtitle = "";
	public static String startdate = "";
	public static String starttime = "";
	public static String endtime = "";
	public static int totaltests = 0;
	public static int totalfailed = 0;
	public static String reportfilepath = "";
	/***********************************/

	//	public static ExtentReports reports;
	//	public static ExtentHtmlReporter htmlReporter;
	//	public static ExtentTest test;

	public static String placeholder_url = "https://google.com";
	public static String enviURL;
	public static String enviURL2;
	public static String enviURL3;
	public static String dburl;
	public static String sessionid;
	public static boolean sendreportinemail = false;
	public static int APICallStatuscode = 0;
	public static String environment;
	/******** Reading Network Data *********/
	//	public static BrowserMobProxy proxy;
	public static Proxy seleniumProxy;








	public static Properties readPropertyFile(String filePath) {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(filePath));
		} catch (Exception e) {
			System.out.println("Exception has Occurred in Method readPropertyFile" + e.getMessage() + e.getStackTrace());

		}
		return prop;
	}






	
}
