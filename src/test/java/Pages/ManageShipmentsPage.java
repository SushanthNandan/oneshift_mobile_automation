package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Utilities.BaseClass;
import Utilities.ObjectHelper;

public class ManageShipmentsPage {
	public static WebDriver webdriver = BaseClass.getwebDriver();

	public static String manageShipmentsTextPath = "//h1[text()='Manage Shipments']";
	public static String createShipmentBtnPath = "//a[@role='button']/span[text()='Create a New Shipment']";
	public static String scrollBtnPath = "//div[div[@id='fuse-navbar']]/div[2]/div/div[2]/div";
	public static String totalShipmentsCountPath = "//div[span='Items per page']/span[2]";
	public static String itemsPerPageValuePath = "//div[span='Items per page']/div/div/div";
	public static String previousBtnPath = "//button[@aria-label='Previous Page']";
	public static String nextBtnPath = "//button[@aria-label='Next Page']";
	public static String statusSortPath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='STATUS']";
	public static String statusFilterPath = "//table[@aria-labelledby='shipments list table']/thead/tr[2]/th[2]/div/div/div/input[@placeholder='Filter']";
	public static String shipmentIdSortPath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='SHIPMENT ID']";
	public static String pickUpDateSortPath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='PICKUP']";
	public static String deliveryDateSortPath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='DELIVERY']";
	public static String showAllPath = "//ul/div/li[1]/label/span/span/input[@value='Show All']";
	public static String pcaStatusSelectPath = "//ul/div/li[3]/label/span/span/input[@value='Pending Carrier Assignment']";
	public static String shipmentIdInputPath = "//input[@id='shipmentId']";

	@FindBy(xpath = "//input[@id='1shift-ms-stf-shipmentId']")
	public static WebElement shipmentIdInput;

	@FindBy(xpath = "//h1[text()='Manage Shipments']")
	public static WebElement manageShipmentsText;

	@FindBy(xpath = "//a[@role='button']/span[text()='Create a New Shipment']")
	public static WebElement createShipmentBtn;

	@FindBy(xpath = "//div[div[@id='fuse-navbar']]/div[2]/div/div[2]/div")
	public static WebElement scrollBtn;

	@FindBy(xpath = "//div[span='Items per page']/span[2]")
	public static WebElement totalShipmentsCount;

	@FindBy(xpath = "//div[span='Items per page']/div/div/div")
	public static WebElement itemsPerPageValue;

	@FindBy(xpath = "//button[@aria-label='Previous Page']")
	public static WebElement previousBtn;

	@FindBy(xpath = "//button[@aria-label='Next Page']")
	public static WebElement nextBtn;

	@FindBy(xpath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='STATUS']")
	public static WebElement statusSort;

	@FindBy(xpath = "//table[@aria-labelledby='shipments list table']/thead/tr[2]/th[2]/div/div/div/input[@placeholder='Filter']")
	public static WebElement statusFilter;

	@FindBy(xpath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='SHIPMENT ID']")
	public static WebElement shipmentIdSort;

	@FindBy(xpath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='PICKUP']")
	public static WebElement pickUpDateSort;

	@FindBy(xpath = "//table[@aria-labelledby='shipments list table']/thead/tr[1]/th/span/strong[text()='DELIVERY']")
	public static WebElement deliveryDateSort;

	@FindBy(xpath = "//ul/div/li[1]/label/span/span/input[@value='Show All']")
	public static WebElement showAll;

	//Review shipment Objects

	@FindBy(xpath="//input[@id='shipmentId']")
	public static WebElement SearchShipment;

	@FindBy(xpath="//span[text()='Pending Trucker Assignment']//parent::td")
	public static WebElement OpenShipment;

	@FindBy(xpath="//span[text()='Shipment ID']//parent::div//p")
	public static WebElement getShipmentId;

	@FindBy(xpath="//span[text()='Edit']")
	public static List<WebElement> editButton;

	@FindBy(xpath="//a[text()='Save']")
	public static WebElement SaveButton;

	@FindBy(xpath="//a[text()='Cancel']")
	public static WebElement CancelButton;

	@FindBy(xpath = "//input[@name='description']")
	public static WebElement CommodityName;

	@FindBy(xpath="//input[@name='company_name']")
	public static WebElement companyName;

	@FindBy(xpath="//p[text()='Enter the name of the carrier']//parent::div")
	public static List<WebElement> SelectCarrier;

	@FindBy(xpath="//p[text()='Enter the name of the carrier']")
	public static WebElement EnterCarrier;

	@FindBy(xpath="//span[text()='Assign Carrier']")
	public static WebElement AssignCarrierButton;

	@FindBy(xpath="//span[text()='Assign Carrier']//preceding::p[1]")
	public static WebElement assignCarrier;

	@FindBy(xpath="//span[text()='Assign Carrier']//preceding::input[1]")
	public static WebElement clickOnAssignCarrier;

	@FindBy(xpath="//*[@id='btn-notification-dropdown']/span")
	public static WebElement notification;

	@FindBy(xpath="//b[contains(text(), ' LLB')]")
	public static List<WebElement> ClickOnShipment;

	@FindBy(xpath="//span[text()='Accept Shipment']")
	public static WebElement AcceptButton;

	@FindBy(xpath="//span[text()='Shipment successfully Accepted']")
	public static WebElement SuccessMessage;

	@FindBy(xpath="//div[@id='select-driver']")
	public static WebElement SelectDriver;

	@FindBy(xpath="//span[text()='Assign']")
	public static WebElement AssignButton;

	@FindBy(xpath="//span[text()='Dispatch']")
	public static WebElement DispatchButton;

	
	@FindBy(xpath = "//table[@aria-labelledby='shipments list table']/tbody/tr[2]/td[12]/button/span")
	public static WebElement dots;
	@FindBy(xpath = "//ul/li[@id='1shift-ms-str-duplicate-shipment']")
	public static WebElement duplicateShipmentBtn;

	@FindBy(xpath = "//ul/div/li[3]/label/span/span/input[@value='Pending Carrier Assignment']")
	public static WebElement pcaStatusSelect;

	public static void scrollDown() {
		WebElement scroll = webdriver.findElement(By.xpath(ManageShipmentsPage.scrollBtnPath));
		scroll.sendKeys(Keys.PAGE_DOWN);
	}

	public static void scrollUP() {
		WebElement scroll = webdriver.findElement(By.xpath(ManageShipmentsPage.scrollBtnPath));
		scroll.sendKeys(Keys.PAGE_UP);
	}

	public ManageShipmentsPage(WebDriver webdriver) {
		ManageShipmentsPage.webdriver = webdriver;
		PageFactory.initElements(webdriver, this);

	}
}