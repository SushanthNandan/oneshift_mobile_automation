Feature: Pick_Drop functionality

		
Scenario: Story6333, Story6370 Update the Next Destination page and remove pick/drop numbering
	Given the app is Installed and login as shippercarrier
	Then I verify Sign In Title for app
	When I create duplicate shipment in web page
	And I disable Capture Commodity Images 
	And I assign carrier and dispatch 	
	Then I verify Dispatched status in logs
	When I login to app
	Then I verify app is in home page
	When I click on Begin Shipment
	Then I verify app is in "SELECT YOUR DESTINATION" page
	And I verify pick company name 
	And I verify drop company name 	
	When I select "SHIPMENT DETAILS" screen 
	And I verify pick company name 
	And I verify drop company name 
	When I close shipment details screen	
	When I select first pick/drop	
	Then I verify i am in "CHECKED IN" screen 
	And I verify pick company name
	When I select back icon
	Then I verify app is in "SELECT YOUR DESTINATION" page
	When I select first pick/drop	
	Then I verify i am in "CHECKED IN" screen
	When I click on confirm button in checked in screen
	Then I verify i am in "FINISHED LOADING" finish screen
	When I click on confirm button in finished loading screen
	And I verify pick company name
	When I click on next in pick/drop complete screen	
	Then I verify app is in "SELECT YOUR DESTINATION" page
	When I select first pick/drop
	Then I verify i am in "CHECKED IN" screen
	And I verify drop company name 	
	When I click on confirm button in checked in screen
	Then I verify i am in "FINISHED UNLOADING" finish screen
	Then I verify checked in status in logs
	When I click on upload POD in finished unloading screen
	And I select to upload image
	When I capture photo
	And I select captured photo
	When I upload photo
	And I verify drop company name 	
	When I click on next in pick/drop complete screen
	Then I verify app is in "COMPLETE" screen
	Then I Complete the shipment successfully
	
		
	
	
	
	
		