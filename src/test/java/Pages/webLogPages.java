package Pages;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import Utilities.CommonFunctions;
import io.appium.java_client.android.AndroidDriver;

public class webLogPages {
	WebDriver webdriver;
	static SoftAssert softassert = new SoftAssert();


	@FindBy(xpath = "//td/span[contains(text(),'Dispatched')]")
	public static WebElement dispatch;
	@FindBy(xpath = "//td/span[contains(text(),'On duty')]")
	public static WebElement driver_started;
	@FindBy(xpath = "//td/span[contains(text(),'In Transit')]")
	public static WebElement in_transit;
	@FindBy(xpath = "//td/span[contains(text(),'Checked-in')]")
	public static WebElement checked_in;
	@FindBy(xpath = "//td/span[contains(text(),'Finished')]")
	public static WebElement finished_loading;
	@FindBy(xpath = "//td/span[contains(text(),'POD Added')]")
	public static List<WebElement> pod_added;
	//	public static WebElement pod_added;
	@FindBy(xpath = "//td/span[contains(text(),'Completed')]")
	public static WebElement completed;
	@FindBy(xpath = "//div/span[text()='Completed']")
	public static WebElement completed_status;
	@FindBy(xpath = "//td/span[contains(text(),'Exception')]")
	public static WebElement exception;
	@FindBy(xpath = "//div/span[contains(text(),'Total Exception')]")
	public static WebElement total_exceptions;	
	@FindBy(xpath = "//td/span[contains(text(),'Off duty')]")
	public static WebElement offDuty;	
	@FindBy(xpath = "//td/span[contains(text(),'On duty')]")
	public static WebElement onDuty;
	@FindBy(xpath = "(//span[text()='Running Late'])[2]")
	public static WebElement running_late;	
	@FindBy(xpath = "//span[text()='late']")
	public static WebElement late;
	
	@FindBy(xpath="//span[text()='Commodity Image(s) Uploaded']")
	public static List<WebElement> commodityTextImages;

	public webLogPages(WebDriver driver) {
		this.webdriver = driver;
		PageFactory.initElements(driver, this);
	}

	public void verify_dispatched() {
		CommonFunctions.scrolltoElement(dispatch);
		softassert.assertTrue(dispatch.isDisplayed(), "Dispatched status is not updated in logs");
		System.out.println("Dispatch status is updated in logs");

	}

	public void verify_driverstarted() {
		CommonFunctions.scrolltoElement(driver_started);
		if(driver_started.isDisplayed())
			System.out.println("Driver has  started");	
		else {
			webdriver.navigate().refresh();
			softassert.assertTrue(driver_started.isDisplayed(), "driver has not started");
		}

	}

	public void verify_intransit() {
		CommonFunctions.scrolltoElement(in_transit);
		softassert.assertTrue(in_transit.isDisplayed(), "Driver is not in transit");
		System.out.println("Driver is in transit");	

	}

	public void verify_checkedin() {
		CommonFunctions.scrolltoElement(checked_in);
		softassert.assertTrue(checked_in.isDisplayed(), "Driver has not checked in");	
		System.out.println("Driver checked in");	

	}

	public void verify_finished() {
		CommonFunctions.scrolltoElement(finished_loading);
		softassert.assertTrue(finished_loading.isDisplayed(),  "Driver has not finished loading/unloading");	
	}

	public void verify_completed() {
		CommonFunctions.waitForVisiblity(completed_status, 20);
		softassert.assertTrue(completed_status.isDisplayed(), "Shipment is not completed");	
	}	

	public void verify_exception() {
		CommonFunctions.scrolltoElement(exception);
		assertTrue(exception.isDisplayed());	
	}

	public void verify_exceptiontype(String exception_type) {
		String text = exception.getText();
		assertTrue(text.contains(exception_type));	
	}

	public void verify_NoPOD() {
		CommonFunctions.scrolltoElement(finished_loading);
		Boolean isdisplayed = pod_added.size() > 0;
		//		boolean isdisplayed = false;
		//		try{
		//			isdisplayed = pod_added.isDisplayed();
		//		}catch (NoSuchElementException e) {
		//			System.out.println(e);
		//		}
		assertFalse(isdisplayed);

	}

	public void verify_POD() {
		CommonFunctions.scrolltoElement(finished_loading);
		Boolean isdisplayed = pod_added.size() > 0;
		assertTrue(isdisplayed);
	}
	
	public void verify_commodityImages_log(int imagecount) {
		commodityTextImages = webdriver.findElements(By.xpath("//span[text()='Commodity Image(s) Uploaded']"));
		CommonFunctions.waitForVisiblity(commodityTextImages.get(imagecount), 10);
		CommonFunctions.scrolltoElement(commodityTextImages.get(imagecount));
		softassert.assertTrue(commodityTextImages.get(imagecount).isDisplayed(),  "Driver has not uploaded commodity Images");	

	}

	public void verify_offdutystatus() {
		CommonFunctions.scrolltoElement(offDuty);
		assertTrue(offDuty.isDisplayed());
	}

	public void verify_ondutystatus() {
		CommonFunctions.scrolltoElement(onDuty);
		assertTrue(onDuty.isDisplayed());
	}

	public void verify_totalExceptions() {
		CommonFunctions.scrolltoElement(total_exceptions);
		assertTrue(total_exceptions.isDisplayed());
	}

	public void verify_running_lateflag() {
		assertTrue(running_late.isDisplayed());
	}

	public void verify_lateflag() {
		assertTrue(late.isDisplayed());
	}
}
