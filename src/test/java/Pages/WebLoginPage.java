package Pages;

import static org.testng.Assert.assertEquals;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import Utilities.CommonFunctions;
import properties.Configprops;


public class WebLoginPage {

	static WebDriver webdriver;
	static SoftAssert softassert = new SoftAssert();
	public static String webdispatch_number = "";
	//	static WebDriver webdriver;
	public static String loginPageTextPath = "//h6[contains(.,'LOGIN TO YOUR ACCOUNT')]";
	public static String emailTextFieldPath = "//input[@id='email']";
	public static String passwordTextFieldPath = "//input[@id='password']";
	public static String forgotPasswordLinkPath = "//a[contains(.,'Forgot Password?')]";
	public static String LoginButtonPath = "//span[contains(.,'Sign In')]";
	public static String createanAccountLinkPath = "//a[contains(.,'Create an account')]";
	public static String manageShipmentsPath = "//span[contains(.,'Manage Shipments')]";



	@FindBy(xpath = "//*[contains(text(), \"Manage Shipments\")]")
	public static WebElement manageShipments;
	@FindBy(xpath = "//h6[contains(.,'LOGIN TO YOUR ACCOUNT')]")
	public static WebElement loginPageText;

	@FindBy(xpath = "//h1[contains(.,'Login')]")
	public static WebElement loginPageTitle;

	@FindBy(xpath = "//input[@id='1shift-l-f-email-field']")
	public static WebElement emailtextfield;
	@FindBy(xpath = "//input[@id='1shift-l-f-password-field']")
	public static WebElement passwordTextField;
	@FindBy(xpath = "//span[text()='Log In']")
	public static WebElement signInButton;

	@FindBy(xpath = "//a[contains(.,'Forgot Password?')]")
	public static WebElement forgotPasswordLink;


	@FindBy(xpath = "//a[contains(.,'Create an account')]")
	public static WebElement createanAccountLink;

	@FindBy(xpath = "//div[@id ='btn-navigation-dashboard-text']/p")
	public static WebElement DashBoard;

	@FindBy(xpath = "//span[contains(.,'Exceptions')]")
	public static WebElement exception;

	@FindBy(xpath = "//span[contains(.,'Admin')]")
	public static WebElement admin;

	@FindBy(xpath = "//span[text()='keyboard_arrow_down']")
	public static WebElement arrow;

	@FindBy(xpath = "//span[text()='Sign out']")
	public static WebElement singOut;

	@FindBy(xpath = "//span[text()='Company Info.']")
	public static WebElement companyinfo;	

	@FindBy(xpath = "//input[@name='dispatchPhoneNumber']")
	public static WebElement dispatch_num;		

	@FindBy(xpath = "//span[text()='Create a New Shipment']")
	public static WebElement createaNewShipment;

	public WebLoginPage(WebDriver driver) {
		this.webdriver = driver;
		PageFactory.initElements(driver, this);
	}

	public void logIn() throws Exception {
		emailtextfield.sendKeys(Configprops.shipperemail);
		passwordTextField.sendKeys(Configprops.shipperpassword);
		CommonFunctions.clickUsingJavaExecutor(signInButton);
		softassert.assertEquals(CommonFunctions.waitForVisiblity(DashBoard, 10), true, "Dashboard page is not displayed");
	}

	public static void logout() throws Exception {
		CommonFunctions.clickUsingJavaExecutor(arrow);
		CommonFunctions.clickUsingJavaExecutor(singOut);
		if (CommonFunctions.waitForVisiblity(loginPageTitle, 30)) {
			softassert.assertEquals(true, true);
			System.out.println("User logged out successfully");
		} else {
			assertEquals(false, true);
		}
	}


	public static void logInCarrier() throws Exception {
		emailtextfield.sendKeys(Configprops.carrieremail);
		passwordTextField.sendKeys(Configprops.carrierpassword);
		CommonFunctions.clickUsingJavaExecutor(signInButton);
		CommonFunctions.waitandClick(WebCreateShipment.Shipments, 30);
		CommonFunctions.clickUsingJavaExecutor(WebCreateShipment.Shipments);
		softassert.assertTrue(manageShipments.isDisplayed(), "Carrier user is not logged in");
		System.out.println("Logged in as carrier");
	}

	public void companyinfo() {
		CommonFunctions.clickUsingJavaExecutor(arrow);
		companyinfo.click();
	}

	public void getdispatch_num() {
		webdispatch_number = dispatch_num.getAttribute("value");
		System.out.println("Web number..."+webdispatch_number);
	}


}
