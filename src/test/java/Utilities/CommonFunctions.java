package Utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;


public class CommonFunctions {

	public static void waitandClick(By locator, int waitTime) throws Exception {
		WebElement element;

		WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);

		scrolltoElement(wait.until(ExpectedConditions.visibilityOfElementLocated(locator)));

		element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		clickUsingJavaExecutor(element);

	}

	public static void waitandClear(By locator, int waitTime) throws Exception {
		WebElement element;

		WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);

		scrolltoElement(wait.until(ExpectedConditions.visibilityOfElementLocated(locator)));

		element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		clearUsingJavaExecutor(element);

	}

	public static boolean waitForVisiblity(By locator, int waitTime) {
		@SuppressWarnings("unused")
		WebElement element;
		boolean result = false;
		try {
			ObjectHelper.webdriver.switchTo().activeElement();

			WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);

			scrolltoElement(wait.until(ExpectedConditions.visibilityOfElementLocated(locator)));

			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			result = true;
		} catch (Exception e) {

		}
		return result;
	}

	public static void waitandClick(WebElement element, int waitTime) throws Exception {
		ObjectHelper.webdriver.switchTo().activeElement();
		scrolltoElement(element);
		WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
		element = wait.until(ExpectedConditions.elementToBeClickable(element));
		clickUsingJavaExecutor(element);
	}

	public static void waitandClear(WebElement element, int waitTime) throws Exception {
		WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
		element = wait.until(ExpectedConditions.elementToBeClickable(element));
		clearUsingJavaExecutor(element);
	}

	public static boolean waitForVisiblity(WebElement element, int waitTime) {
		boolean result = false;
		try {
			ObjectHelper.webdriver.switchTo().activeElement();

			scrolltoElement(element);

			WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
			// element = wait.until(ExpectedConditions.elementToBeClickable(element));
			element = wait.until(ExpectedConditions.visibilityOf(element));
			result = true;
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	public static void clickonmodalpopup(WebElement element) throws Exception {

		ObjectHelper.webdriver.switchTo().activeElement();

		Thread.sleep(2000);

		waitandClick(element, 30);

		ObjectHelper.webdriver.switchTo().activeElement();
	}

	public static void clickonmodalpopup(String pathstring) throws Exception {

		ObjectHelper.webdriver.switchTo().activeElement();

		Thread.sleep(2000);

		waitandClick(By.xpath(pathstring), 30);

		ObjectHelper.webdriver.switchTo().activeElement();
	}

	public static boolean clickonmodalpopup(String pathstring, String messagepath) throws Exception {
		boolean result = false;

		ObjectHelper.webdriver.switchTo().activeElement();

		Thread.sleep(2000);

		if (waitForVisiblity(By.xpath(messagepath), 10)) {
			result = true;
		}

		waitandClick(By.xpath(pathstring), 30);

		ObjectHelper.webdriver.switchTo().activeElement();

		return result;
	}

	public static void chainclicksonpopupOK() throws Exception {
		try {
			while (waitForVisiblity(By.xpath("//button[text()='OK']"), 3)) {
				// waitandClick(By.xpath("//button[text()='OK']"), 5);
				ObjectHelper.webdriver.switchTo().activeElement();

				WebElement element = ObjectHelper.webdriver.findElement(By.xpath("//button[text()='OK']"));
				JavascriptExecutor js = (JavascriptExecutor) ObjectHelper.webdriver;
				js.executeScript("arguments[0].click();", element);

				ObjectHelper.webdriver.switchTo().activeElement();
			}
		} catch (NoSuchElementException ne) {
		} catch (TimeoutException te) {
		}

	}

	public static void chainclicksonpopupOKAndClose() throws Exception {
		try {
			while (waitForVisiblity(By.xpath("//button[text()='OK']"), 10)) {

				// waitandClick(By.xpath("//button[text()='OK']"), 5);
				ObjectHelper.webdriver.switchTo().activeElement();

				WebElement element = ObjectHelper.webdriver.findElement(By.xpath("//button[text()='OK']"));
				JavascriptExecutor js = (JavascriptExecutor) ObjectHelper.webdriver;
				js.executeScript("arguments[0].click();", element);

				waitForVisiblity(By.xpath("//button[@ng-click='closeDeleted()']"), 3);
				ObjectHelper.webdriver.switchTo().activeElement();

				WebElement element1 = ObjectHelper.webdriver.findElement(By.xpath("//button[@ng-click='closeDeleted()']"));
				JavascriptExecutor js1 = (JavascriptExecutor) ObjectHelper.webdriver;
				js1.executeScript("arguments[0].click();", element1);

				ObjectHelper.webdriver.switchTo().activeElement();
			}

		} catch (NoSuchElementException ne) {
		} catch (TimeoutException te) {
		}

	}

	public static void chainclicksonpopup(WebElement element) throws Exception {
		try {
			while (waitForVisiblity(element, 10)) {

				// waitandClick(By.xpath("//button[text()='OK']"), 5);
				ObjectHelper.webdriver.switchTo().activeElement();

				JavascriptExecutor js = (JavascriptExecutor) ObjectHelper.webdriver;
				js.executeScript("arguments[0].click();", element);
			}
		} catch (NoSuchElementException ne) {
		} catch (TimeoutException te) {
		}

	}

	public static String getcomboboxselectedvalue(WebElement element) throws Exception {
		String value = null;

		Select combolist = new Select(element);
		value = combolist.getFirstSelectedOption().getText();

		return value;
	}

	public static String getexception(Exception e) {
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	public static void clickUsingJavaExecutor(String xpath) {
		WebElement element = ObjectHelper.webdriver.findElement(By.xpath(xpath));

		scrolltoElement(element);

		JavascriptExecutor js = (JavascriptExecutor) ObjectHelper.webdriver;
		js.executeScript("arguments[0].click();", element);
	}

	public static void clickUsingJavaExecutor(WebElement element) {
		scrolltoElement(element);

		JavascriptExecutor js = (JavascriptExecutor) ObjectHelper.webdriver;
		js.executeScript("arguments[0].click();", element);
	}

	public static void clearUsingJavaExecutor(String xpath) {
		WebElement element = ObjectHelper.webdriver.findElement(By.xpath(xpath));
		scrolltoElement(element);
		JavascriptExecutor js = (JavascriptExecutor) ObjectHelper.webdriver;
		js.executeScript("arguments[0].value=''", element);
	}

	public static void clearUsingJavaExecutor(WebElement element) throws Exception {
		scrolltoElement(element);
		JavascriptExecutor js = (JavascriptExecutor) ObjectHelper.webdriver;
		js.executeScript("arguments[0].value=''", element);
	}

	public static void scrolltoElement(WebElement element) {
		try {
			((JavascriptExecutor) ObjectHelper.webdriver).executeScript("arguments[0].scrollIntoView(false);", element);
			Thread.sleep(500);
		} catch (Exception e) {
		}
	}

	public static void clickonmodalpopup(By locator) throws Exception {

		ObjectHelper.webdriver.switchTo().activeElement();

		Thread.sleep(2000);

		waitandClick(locator, 30);

		ObjectHelper.webdriver.switchTo().activeElement();
	}

	public static boolean moveFile(String currentFileDirectory, String fileName, String newFileDirectory) {
		File file1 = new File(new File(".", currentFileDirectory + "//" + fileName).getAbsolutePath());
		File file2 = new File(new File(".", newFileDirectory + "//" + fileName).getAbsolutePath());
		String fName[] = fileName.split("\\.");
		String name;
		int i = 0;
		while (file2.exists()) {
			i++;
			name = fName[0] + i + "." + fName[1];
			file2 = new File(new File(".", newFileDirectory + "//" + name).getAbsolutePath());
		}
		if (file1.renameTo(file2)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean copyFile(String sourceFileDirectory, String fileNameWithExtn, String newFileDirectory) {
		try {
			File srcFile = new File(new File(".", sourceFileDirectory + "//" + fileNameWithExtn).getAbsolutePath());
			File destFile = new File(new File(".", newFileDirectory + "//" + fileNameWithExtn).getAbsolutePath());
			FileUtils.copyFile(srcFile, destFile);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean copyFileNewName(String sourceFileDirectory, String fileNameWithExtn, String newFileDirectory,
			String newFileNameWithExtn) {
		try {
			File srcFile = new File(new File(".", sourceFileDirectory + "//" + fileNameWithExtn).getAbsolutePath());
			File destFile = new File(new File(".", newFileDirectory + "//" + newFileNameWithExtn).getAbsolutePath());
			FileUtils.copyFile(srcFile, destFile);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean waitForDownload(String fileName) {
		boolean err = false;
		try {
			Wait<WebDriver> fluentWait1 = new FluentWait<WebDriver>(ObjectHelper.webdriver)
					.withTimeout(Duration.ofSeconds(40)).pollingEvery(Duration.ofSeconds(2));
			final File file1 = new File(
					new File(".", ObjectHelper.downloadsFolder + "//" + fileName).getAbsolutePath());
			Function<WebDriver, Boolean> func1 = new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver arg0) {
					return file1.exists();
				}
			};
			fluentWait1.until(func1);
		} catch (Exception e) {
			err = true;
		}
		return err;
	}

	public static boolean archiveFile(String currentFileDirectory, String fileName) {
		File file1 = new File(new File(".", currentFileDirectory + "//" + fileName).getAbsolutePath());
		File file = new File(new File(".", currentFileDirectory + "//Archive").getAbsolutePath());
		if (!file.exists()) {
			file.mkdir();
		}
		File file2 = new File(new File(".", currentFileDirectory + "//Archive" + "//" + fileName).getAbsolutePath());
		String fName[] = fileName.split("\\.");
		String name;
		int i = 0;
		while (file2.exists()) {
			i++;
			name = fName[0] + i + "." + fName[1];
			file2 = new File(new File(".", currentFileDirectory + "//Archive" + "//" + name).getAbsolutePath());
		}
		if (file1.renameTo(file2)) {
			return true;
		} else {
			return false;
		}
	}

	public static void waitForVisiblityWithException(By locator, int waitTime) {
		WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public static void waitForVisiblityWithException(WebElement element, int waitTime) {
		ObjectHelper.webdriver.switchTo().activeElement();

		scrolltoElement(element);

		WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
		element = wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static void createFolder(String folderName) {
		File folder = new File(new File(".", "//" + folderName).getAbsolutePath());
		if (!folder.exists()) {
			folder.mkdir();
		}
	}

	/*
	 * parameters - could be dd or mm or yyyy or dd/mm etc
	 */
	public static String getToday(String formatddmm) {
		SimpleDateFormat formatter = new SimpleDateFormat(formatddmm);
		Date date = new Date();
		return formatter.format(date);
	}

	public static String getDayName(int adddays) {
		String dayname = null;

		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.DAY_OF_YEAR, adddays);

		Date date = calendar.getTime();

		dayname = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

		return dayname;
	}

	public static String getDayNamefromDate(String stringdate) throws ParseException {
		String dayname = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

		Date date = formatter.parse(stringdate);

		dayname = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

		return dayname;
	}

	public static String getDate(int adddays) {
		String dayname = null;

		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.DAY_OF_YEAR, adddays);

		Date date = calendar.getTime();

		dayname = new SimpleDateFormat("d-MMM-yyyy", Locale.ENGLISH).format(date.getTime());

		return dayname;
	}

	public static String getDate(int adddays, String format) {
		String dayname = null;

		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.DAY_OF_YEAR, adddays);

		Date date = calendar.getTime();

		dayname = new SimpleDateFormat(format, Locale.ENGLISH).format(date.getTime());

		return dayname;
	}

	public static String getDateNoname(int adddays) {
		String dayname = null;

		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.DAY_OF_YEAR, adddays);

		Date date = calendar.getTime();

		dayname = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date.getTime());

		return dayname;
	}

	public static void main(String[] args) {
		try {
			ObjectHelper.sessionid = "ASP.NET_SessionId=lhya4minphp5g1akh0q0a1zc";
			// JSONArray jsonarray = new JSONArray();
			// jsonarray = CommonFunctions.getJsonResponse("GET",
			// "http://10.1.1.139/Location/GetSites?address=vancouver&openEarly=false&openSundays=false&wheelChairAccessible=false&doesECG=false&holterMonitoring=false&bloodPressureMonitoring=false&serveAutism=false&getCheckedOnline=false&openSaturdays=false");
			//
			// for (int k = 0; k < jsonarray.length(); k++) {
			// JSONObject objectname = new JSONObject(jsonarray.get(k).toString());
			//
			// System.out.println("Site ID : " + objectname.getInt("SiteID"));
			// }

			ArrayList<String> availabledatelist = new ArrayList<String>();

			// JSONArray availabeldatesarray = CommonFunctions.getJsonResponse("GET",
			// "http://10.1.1.139/AppointmentBooking/GetMonthViewAvailableAppointmentDays?siteID=13&selectedDate=Thu%20"
			// + CommonFunctions.getDate(0).split("-")[1]
			// + "%2009%202018%2017:19:38%20GMT-0700%20(Pacific%20Daylight%20Time)");
			//
			// for (int k = 0; k < availabeldatesarray.length(); k++) {
			//
			// // System.out.println(availabeldatesarray.getString(k).toString());
			// }
			// "HH:m - dd/mm/yyyy", ship.getData()[i].getTimestamp(), "PST")
			// System.out.println(CommonFunctions.formatTimewithZoneChange("HH:m -
			// dd/mm/yyyy", "2018-10-22T00:09:01.000Z", "PST"));
			// System.out.println(CommonFunctions.convertUTCtime("PST",
			// "2018-10-22T00:09:01.000Z"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//	public static JSONArray getJsonResponse(String requesttype, String url) throws Exception {
	//		// JSONObject jsonresult = new JSONObject();
	//		JSONArray jsonarray = new JSONArray();
	//		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	//
	//		WebHandler.getSessionID();
	//
	//		try {
	//			HttpResponse response;
	//			if (requesttype.equals("POST")) {
	//				HttpPost request = new HttpPost(url.replace(" ", "+"));
	//				request.addHeader("content-type", "application/json");
	//				request.setHeader("Cookie", ObjectHelper.sessionid);
	//				response = httpClient.execute(request);
	//
	//				HttpEntity entity = response.getEntity();
	//
	//				if (entity != null) {
	//					InputStream instream = entity.getContent();
	//					String result = convertStreamToString(instream);
	//
	//					// jsonresult = new JSONObject(result);
	//					jsonarray = new JSONArray(result);
	//
	//					instream.close();
	//				}
	//			} else if (requesttype.equals("GET")) {
	//				HttpGet request = new HttpGet(url.replace(" ", "+"));
	//				request.addHeader("content-type", "application/json");
	//				request.setHeader("Cookie", ObjectHelper.sessionid);
	//				response = httpClient.execute(request);
	//
	//				// response.getStatusLine().getStatusCode();
	//
	//				HttpEntity entity = response.getEntity();
	//
	//				if (entity != null) {
	//					InputStream instream = entity.getContent();
	//					String result = convertStreamToString(instream);
	//
	//					// jsonresult = new JSONObject(result);
	//					jsonarray = new JSONArray(result);
	//
	//					instream.close();
	//				}
	//			}
	//		} catch (Exception ex) {
	//			throw ex;
	//		} finally {
	//			httpClient.close();
	//		}
	//		return jsonarray;
	//	}

	//	public static JSONObject getJsonResponse(String requesttype, String url, JSONObject json) throws Exception {
	//		JSONObject jsonresult = new JSONObject();
	//		// JSONArray jsonarray = new JSONArray();
	//		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	//
	//		WebHandler.getSessionID();
	//
	//		try {
	//			HttpResponse response;
	//			if (requesttype.equals("POST")) {
	//				StringEntity se = new StringEntity(json.toString());
	//
	//				HttpPost request = new HttpPost(url.replace(" ", "+"));
	//				request.addHeader("content-type", "application/json");
	//				request.setHeader("Cookie", ObjectHelper.sessionid);
	//				request.setEntity(se);
	//				response = httpClient.execute(request);
	//
	//				ObjectHelper.APICallStatuscode = response.getStatusLine().getStatusCode();
	//
	//				HttpEntity entity = response.getEntity();
	//
	//				if (entity != null) {
	//					InputStream instream = entity.getContent();
	//					String result = convertStreamToString(instream);
	//
	//					jsonresult = new JSONObject(result);
	//					// jsonarray = new JSONArray(result);
	//
	//					instream.close();
	//				}
	//			} else if (requesttype.equals("GET")) {
	//				HttpGet request = new HttpGet(url.replace(" ", "+"));
	//				request.addHeader("content-type", "application/json");
	//				request.setHeader("Cookie", ObjectHelper.sessionid);
	//				response = httpClient.execute(request);
	//
	//				ObjectHelper.APICallStatuscode = response.getStatusLine().getStatusCode();
	//
	//				HttpEntity entity = response.getEntity();
	//
	//				if (entity != null) {
	//					InputStream instream = entity.getContent();
	//					String result = convertStreamToString(instream);
	//
	//					jsonresult = new JSONObject(result);
	//					// jsonarray = new JSONArray(result);
	//
	//					instream.close();
	//				}
	//			}
	//		} catch (Exception ex) {
	//			throw ex;
	//		} finally {
	//			httpClient.close();
	//		}
	//		return jsonresult;
	//	}

	private static String convertStreamToString(InputStream is) throws Exception {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/*
	 * DESCRIPTION: Gets message on modal pop up and closes it (created for BCLIS)
	 * PARAMETER: WebElement for message and WebElement for the close button on
	 * modal RETURNS : Message on modal Author: Bhavna Karanjekar Modified By:
	 */
	public static String getMessageOnModalPopupAndClose(WebElement messageElement, WebElement closeElement)
			throws Exception {

		String messageOnModal = "";
		ObjectHelper.webdriver.switchTo().activeElement();

		Thread.sleep(2000);
		if (waitForVisiblity(messageElement, 10)) {
			messageOnModal = messageElement.getText();
		}
		waitandClick(closeElement, 30);
		ObjectHelper.webdriver.switchTo().activeElement();
		return messageOnModal;
	}

	/*
	 * DESCRIPTION: wait for alert and accept (created for BCLIS) PARAMETER: wait
	 * time RETURNS : true of false Author: Bhavna Karanjekar Modified By:
	 */
	public static boolean waitForAlertAndAccept(int waitTime) throws Exception {

		try {
			WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
			wait.until(ExpectedConditions.alertIsPresent());

			ObjectHelper.webdriver.switchTo().alert().accept();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/*
	 * DESCRIPTION: wait for alert and get message on alert (created for BCLIS)
	 * PARAMETER: waitTime RETURNS : Message on alert Author: Bhavna Karanjekar
	 * Modified By:
	 */
	public static String waitForAlertAndGetMessage(int waitTime) throws Exception {

		try {
			WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
			wait.until(ExpectedConditions.alertIsPresent());

			return ObjectHelper.webdriver.switchTo().alert().getText();

		} catch (Exception e) {
			return "error";
		}

	}

	/*
	 * DESCRIPTION: wait for Tab to Open, waits till the expected number of windows
	 * (created for BCLIS) PARAMETER: expected number of windows and wait time
	 * RETURNS : True or false Author: Bhavna Karanjekar Modified By:
	 */
	public static boolean waitForNewTabToOpen(int numberOfWindowsExpected, int waitTime) throws Exception {

		try {
			WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
			wait.until(ExpectedConditions.numberOfWindowsToBe(numberOfWindowsExpected));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public static boolean isElementClickable(WebElement webe) {

		try {
			WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(webe));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void ClearAndSetText(WebElement element, String text) {
		Actions navigator = new Actions(ObjectHelper.webdriver);
		navigator.click(element).sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME).keyUp(Keys.SHIFT)
		.sendKeys(Keys.BACK_SPACE).sendKeys(text).perform();
	}

	public static void sendKeysUsingJavaExecutor(WebElement element, String value) {
		((JavascriptExecutor) ObjectHelper.webdriver).executeScript("arguments[0].value='" + value + "'", element);
	}

	/* incrementing number of days to a date, also can pass required format */
	public static String incrementDate(String date, String format, int incrementDays) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new SimpleDateFormat(format).parse(date));
		cal.add(Calendar.DATE, incrementDays);
		date = new SimpleDateFormat(format).format(cal.getTime());
		return date;
	}

	public static boolean verifyTextBoxData(WebElement element, String expectedText) throws Exception {

		scrolltoElement(element);

		String actualText = element.getAttribute("value");

		if (actualText.equals(expectedText)) {
			return true;
		} else {
			return false;
		}

	}

	public static ArrayList<String> getCountryList() {
		ArrayList<String> country = new ArrayList<String>();
		File[] files = new File("exports" + File.separator + "json" + File.separator + "countries").listFiles();
		// If this pathname does not denote a directory, then listFiles()
		// returns null.
		for (File file : files) {
			if (file.isFile()) {
				String fileName = file.getName();
				int pos = fileName.lastIndexOf(".");
				if (pos > 0) {
					fileName = fileName.substring(0, pos);
					country.add(fileName);
				}

			}
		}
		return country;
	}

	public static boolean checkdateinUTC(String checkdate) throws Exception {
		if (checkdate != null && checkdate.length() > 4
				&& checkdate.substring(checkdate.length() - 4).contains("000Z")) {
			return true;
		} else {
			return false;
		}
	}

	//	public static JSONObject getJsonResponse_1(String requesttype, String url) throws Exception {
	//		JSONObject jsonresult = new JSONObject();
	//		// JSONArray jsonarray = new JSONArray();
	//		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	//		// System.out.println("getJsonResponse_1 1::");
	//		WebHandler.getSessionID();
	//
	//		try {
	//			HttpResponse response;
	//
	//			if (requesttype.equals("GET")) {
	//				// System.out.println("getJsonResponse_1 2::");
	//
	//				HttpGet request = new HttpGet(url.replace(" ", "+"));
	//				// System.out.println("getJsonResponse_1 request::" + request.toString());
	//
	//				request.addHeader("content-type", "application/json");
	//				request.setHeader("Cookie", ObjectHelper.sessionid);
	//
	//				response = httpClient.execute(request);
	//				// System.out.println("getJsonResponse_1 3::");
	//
	//				ObjectHelper.APICallStatuscode = response.getStatusLine().getStatusCode();
	//
	//				HttpEntity entity = response.getEntity();
	//
	//				if (entity != null) {
	//					InputStream instream = entity.getContent();
	//					String result = convertStreamToString(instream);
	//					jsonresult = new JSONObject(result);
	//					// jsonarray = new JSONArray(result);
	//
	//					instream.close();
	//				}
	//			}
	//		} catch (Exception ex) {
	//			throw ex;
	//		} finally {
	//			httpClient.close();
	//		}
	//		return jsonresult;
	//	}

	// System.out.println(CommonFunctions.convertUTCtime("PST",
	// "2018-10-20T00:11:01.000Z"));
	public static String convertUTCtime(String timeZone, String UTCtime) throws Exception {
		String time = null;
		try {
			DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

			Date date = utcFormat.parse(UTCtime);

			DateFormat pstFormat = new SimpleDateFormat("HH:mm -MM/dd/yyyy");
			pstFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

			time = pstFormat.format(date);
		} catch (Exception e) {
			throw e;
		}
		return time;
	}

	public static String formatTimewithZoneChange(String targetformat, String UTCtime, String requiredTimeZone)
			throws Exception {
		String time = null;
		try {
			DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

			Date date = utcFormat.parse(UTCtime);

			DateFormat pstFormat = new SimpleDateFormat(targetformat);
			pstFormat.setTimeZone(TimeZone.getTimeZone(requiredTimeZone));

			time = pstFormat.format(date);
		} catch (Exception e) {
			throw e;
		}
		return time;
	}

	//	public static String getJsonResponse_1(String requesttype, String url, JSONArray json) throws Exception {
	//		JSONObject jsonObject = new JSONObject();
	//		JSONArray jsonArray = new JSONArray();
	//		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	//
	//		WebHandler.getSessionID();
	//		String returnresponse = null;
	//		try {
	//			HttpResponse response;
	//			if (requesttype.equals("POST")) {
	//				StringEntity se = new StringEntity(json.toString());
	//				HttpPost request = new HttpPost(url.replace(" ", "+"));
	//				request.addHeader("content-type", "application/json");
	//				request.setHeader("Cookie", ObjectHelper.sessionid);
	//				request.setEntity(se);
	//				response = httpClient.execute(request);
	//
	//				ObjectHelper.APICallStatuscode = response.getStatusLine().getStatusCode();
	//				HttpEntity entity = response.getEntity();
	//
	//				if (entity != null) {
	//					InputStream instream = entity.getContent();
	//					String result = convertStreamToString(instream);
	//
	//					if (ObjectHelper.APICallStatuscode == 201) {
	//						jsonArray = new JSONArray(result);
	//						returnresponse = jsonArray.toString();
	//					} else {
	//						jsonObject = new JSONObject(result);
	//						returnresponse = jsonObject.toString();
	//
	//					}
	//					instream.close();
	//				}
	//			}
	//			if (requesttype.equals("PATCH")) {
	//				StringEntity se = new StringEntity(json.toString());
	//				HttpPatch request = new HttpPatch(url.replace(" ", "+"));
	//				request.addHeader("content-type", "application/json");
	//				request.setHeader("Cookie", ObjectHelper.sessionid);
	//				request.setEntity(se);
	//				response = httpClient.execute(request);
	//
	//				ObjectHelper.APICallStatuscode = response.getStatusLine().getStatusCode();
	//				HttpEntity entity = response.getEntity();
	//
	//				if (entity != null) {
	//					InputStream instream = entity.getContent();
	//					String result = convertStreamToString(instream);
	//
	//					if (ObjectHelper.APICallStatuscode == 200) {
	//						jsonArray = new JSONArray(result);
	//						returnresponse = jsonArray.toString();
	//					} else {
	//						jsonObject = new JSONObject(result);
	//						returnresponse = jsonObject.toString();
	//
	//					}
	//					instream.close();
	//				}
	//			}
	//
	//		} catch (Exception ex) {
	//			throw ex;
	//		} finally {
	//			httpClient.close();
	//		}
	//		return returnresponse;
	//	}

	/* generate the random alphanumeric string by given length */
	public static String getSaltString(int aKeyLength) {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < aKeyLength) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	/*
	 * generate the random alphanumeric string by given length and value which
	 * doesn't match
	 */
	public static String getSaltStringOnlyInt(int aKeyLength) {
		String SALTCHARS = "1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < aKeyLength) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	/* generate the random number by putting min and max value */
	/*
	 * make sure the min should be small than max. e.g 10000,99999 as there is no
	 * checking for own use
	 */
	public static int randInt(int min, int max) {

		// Usually this can be a field rather than a method variable
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	public static String dateConvertionSourcetoTarget(String sourceformat, String time, String targetformat) {
		String formattedtime = null;
		try {
			SimpleDateFormat utcFormat = new SimpleDateFormat(sourceformat); // format that you send the input string to
			// it.
			Date date = utcFormat.parse(time);
			SimpleDateFormat timeFormat = new SimpleDateFormat(targetformat); // mm/yyyy or the format u want to see the
			// result
			formattedtime = timeFormat.format(date);
		} catch (Exception e) {

			e.printStackTrace();
		}

		return formattedtime;
	}


	public static void waitandNormalClick(WebElement element, int waitTime) throws Exception {
		ObjectHelper.webdriver.switchTo().activeElement();
		scrolltoElement(element);

		WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, waitTime);
		element = wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	public static void waitUntilPageLoads()
	{

		try{
			ExpectedCondition<Boolean>	pageloadcondition=new ExpectedCondition<Boolean>()
			{
				public Boolean apply(WebDriver driver) 
				{
					return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
				}
			};
			WebDriverWait wait = new WebDriverWait(ObjectHelper.webdriver, 50);
			wait.until(pageloadcondition);
		}


		catch(Exception E){

			E.printStackTrace();
			Reporter.log("Unable to wait for the page to load", true);

		}


	}

	public WebElement replaceParameterInXpathWith(WebElement existingXpath,String stringToReplace)
	{
		WebElement webelement = null;

		try{
			//		return WebElement(String.format(existingXpath.toString().replace("By.xpath: ", ""), stringToReplace));
			String.format(existingXpath.toString(), stringToReplace);
			return webelement;

		}catch(Exception e)
		{
			e.printStackTrace();
			Reporter.log("error occured while replacing string value in the xpath");
			return null;
		}
	}

	public static String get_color(WebElement element) throws Exception {
		String actualcolor = element.getCssValue("background-color");
		return 	Color.fromString(actualcolor).asHex();

	}
}
