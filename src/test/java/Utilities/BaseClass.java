package Utilities;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import gherkin.lexer.Is;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import Utilities.ObjectHelper;


public class BaseClass {


	static AndroidDriver<MobileElement> androiddriver;
	private static AppiumDriverLocalService service;
	private static AppiumServiceBuilder builder;
	private static Process process;
	static Properties prop;
	static String app_path;


	public static void appiumSetup() throws MalformedURLException {

		prop = ObjectHelper.readPropertyFile("device-configuration.properties");
		app_path = System.getProperty("user.dir")+"\\src\\test\\resources\\Builds\\1shift-1.1911.3-131.apk";
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("platformName", prop.getProperty("platformName"));
		caps.setCapability("platformVersion", prop.getProperty("platformVersion"));
		caps.setCapability("deviceName", prop.getProperty("deviceName")); //this is based on ur mobile device id
		caps.setCapability("automationName", prop.getProperty("automationName"));
		caps.setCapability("appActivity", prop.getProperty("appActivity"));
		caps.setCapability("appPackage", prop.getProperty("appPackage"));
		caps.setCapability("app", app_path );	
		caps.setCapability("newCommandTimeout", 0);
		caps.setCapability("noReset", false);
		caps.setCapability("fullReset", true);
		caps.setCapability("autoGrantPermissions", true);
	//	caps.setCapability("unicodeKeyboard", true);
	//	caps.setCapability("resetKeyboard", true);

		/*
		 * builder = new AppiumServiceBuilder(); builder.withIPAddress("127.0.0.1");
		 * builder.usingPort(4723); builder.withCapabilities(caps);
		 * builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
		 * builder.withArgument(GeneralServerFlag.LOG_LEVEL,"error");
		 */

		try {
			/*
			 * service = AppiumDriverLocalService.buildService(builder); service.start();

			URL url = new URL(prop.getProperty("URL")); */
			startServer();
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
			androiddriver = new AndroidDriver<MobileElement>(url,caps);
			System.out.println("caps are set.............");
			androiddriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}catch (Exception exp) {
			System.out.println("!Cause is :"+exp.getCause());
			System.out.println("Message is :.."+exp.getMessage());
			exp.printStackTrace();

		}

	}

	@Before
	public static void webdriverSetup() throws Exception {
		System.setProperty("webdriver.chrome.driver",  new File(".", "\\src\\test\\resources\\drivers\\chromedriver.exe").getAbsolutePath());
		HashMap<String, Object> chromePreferences = new HashMap<String, Object>();
		chromePreferences.put("profile.default_content_settings.popups", 0);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePreferences);
		options.addArguments("disable-extensions");
		options.addArguments("--start-maximized");

		ObjectHelper.webdriver = new ChromeDriver(options);
		ObjectHelper.webdriver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		ObjectHelper.webdriver.navigate().to("https://app.qa.np.1shift.io/#/login");
		//			ObjectHelper.webdriver.manage().window().maximize();
		ObjectHelper.webdriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("Initiating Appium drivers...");
		appiumSetup();


	}


	@After
	public void teardown(Scenario scenario) throws IOException, InterruptedException {
		CommonLib.uninstallApp("io.appium.settings");
		if(androiddriver!=null) {
			System.out.println("............Closing driver..........");
			//				service.stop();
			//				ObjectHelper.webdriver.quit();
			//				ObjectHelper.webdriver.close();
			androiddriver.quit();
			Runtime.getRuntime().exec("taskkill /f /im cmd.exe") ;
			stopAppiumServer();
			System.out.println("............Closed driver..........");

		}


	}

	public static AndroidDriver<MobileElement> getDriver() {
		return androiddriver;

	}

	public static WebDriver getwebDriver() {
		return ObjectHelper.webdriver;
	}

	
	public static void startServer() {
		Runtime runtime = Runtime.getRuntime();
		try {
			runtime.exec("cmd.exe /c start cmd.exe /k \"appium -a 127.0.0.1 -p 4723 --session-override \"");
			Thread.sleep(10000);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static void stopAppiumServer() throws IOException{
		if(process!=null) 
			process.destroy();
		Runtime.getRuntime().exec("taskkill /f /im cmd.exe");
		System.out.println("Appium server stopped");

	}


}
