package Utilities;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.mobile.NetworkConnection;
import org.openqa.selenium.mobile.NetworkConnection.ConnectionType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.PointOption;
import static io.appium.java_client.touch.offset.PointOption.point;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;

public class CommonLib {

	static AndroidDriver<MobileElement> androiddriver = BaseClass.getDriver();


	public static void InstallApp() {
		System.out.println("tetttt");
	}

	public static void uninstallApp(String packageName) {
		androiddriver.removeApp(packageName);
	}


	public static void waitforelement(AppiumDriver<MobileElement> androiddriver, MobileElement element, int time) {
		WebDriverWait wait = new WebDriverWait(androiddriver, time);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public static void waitandclick(AppiumDriver<MobileElement> androiddriver, MobileElement element, int time) {
		waitforelement(androiddriver, element,time);
		element.click();

	}
	public static void launch_app() {
		androiddriver.launchApp();
		((AndroidDriver) androiddriver).currentActivity();
	}

	public static void setLocation(double latitude, double longitude) {
		Location location = new Location(latitude,longitude,909);
		androiddriver.setLocation(location);

	}

	public static void takeScreenShot(String scname) throws IOException, InterruptedException  {

		TakesScreenshot screenShot = ((TakesScreenshot)androiddriver);
		File screenshotAs =screenShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(5000);
		FileUtils.copyFile(screenshotAs, new File("C:\\AutomationScreenshots\\"+scname+".png"));
	}

	public static void longpress(AndroidDriver driver, WebElement we){
		TouchAction action = new TouchAction(driver);
		action.longPress((LongPressOptions) we).perform();;
	}

	public static void wifiOff() throws InterruptedException {
		NetworkConnection mobileDriver = (NetworkConnection) androiddriver;
		if (mobileDriver.getNetworkConnection() != ConnectionType.AIRPLANE_MODE) {
			// enabling Airplane mode
			mobileDriver.setNetworkConnection(ConnectionType.AIRPLANE_MODE);
		}
	}


	  public static void scroll() {
		  Dimension size = androiddriver.manage().window().getSize();
	        int width = (int) (size.width * 0.5);
	        int startPoint = (int) (size.height * 0.7);
	        int endPoint = (int) (size.height * 0.2);
		  TouchAction action = new TouchAction(androiddriver);
		  action.press(point(width,startPoint)).waitAction(waitOptions(ofMillis(1000)))
          .moveTo(point(width, endPoint))
          .release().perform();
		  		  		  
	  }


	
	public static void scrollAndClick(String visibleText) {
		androiddriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+visibleText+"\").instance(0))");
	        }
	    

	public static void clearappdata() {
		androiddriver.resetApp();
	}

}
