Feature: CommodityImages Flow
	

Scenario: Story-5905, Story-5894 upload commodity images and  Show shipment settings in details
	Given the app is Installed and login as shippercarrier
	Then I verify Sign In Title for app
	When I create duplicate shipment in web page
	And I assign carrier and dispatch 	
	Then I verify Dispatched status in logs
	When I login to app
	Then I verify app is in home page
	When I click on Begin Shipment
	Then I verify app is in "SELECT YOUR DESTINATION" page
	When I select "SHIPMENT DETAILS" screen 
	Then verify "Require Commodity Images" title
	When I close shipment details screen	
	When I select first pick/drop	
	Then I verify i am in "CHECKED IN" screen
	When I select "SHIPMENT DETAILS" screen 
	Then verify "Require Commodity Images" title	
	When I close shipment details screen	
	When I click on confirm button in checked in screen
	Then I verify app is in commodity "UPLOAD PICTURES" page
	When I select to upload image
	When I capture photo
	And I cancel captured photo
	Then I verify app is in commodity "UPLOAD PICTURES" page
	When I select to upload image
	When I capture photo
	And I select captured photo	
	And I upload photo
	Then I verify i am in "FINISHED LOADING" finish screen
	And I verify Commodity images status 0 in logs	
	When I click on confirm button in finished loading screen
	When I click on next in pick/drop complete screen	
	Then I verify app is in "SELECT YOUR DESTINATION" page
	When I select first pick/drop
	Then I verify i am in "CHECKED IN" screen
	When I click on confirm button in checked in screen
	Then I verify app is in commodity "UPLOAD PICTURES" page
	When I select to upload image
	When I capture photo
	And I select captured photo	
	And I upload photo		
	Then I verify i am in "FINISHED UNLOADING" finish screen
	And I verify Commodity images status 1 in logs	
	When I click on upload POD in finished unloading screen
	And I select to upload image
	When I capture photo
	And I select captured photo
	When I upload photo
	When I click on next in pick/drop complete screen
	Then I verify app is in "COMPLETE" screen
	Then I verify shipment completed status in logs
	Then I Complete the shipment successfully	
	
	

	
		