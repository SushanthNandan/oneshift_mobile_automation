package StepDefinition;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import Pages.LoginPage;
import Pages.WebCreateShipment;
import Pages.WebLoginPage;
import Pages.homePAgeObjects;
import Utilities.BaseClass;
import Utilities.CommonFunctions;
import Utilities.CommonLib;
import cucumber.api.java.en.*;


public class Login_stepDef {
	WebDriver webdriver;
	protected LoginPage loginPage = new LoginPage(BaseClass.getDriver());
	protected WebLoginPage webloginPage = new WebLoginPage(BaseClass.getwebDriver());
	static WebCreateShipment createshipment = new WebCreateShipment(BaseClass.getwebDriver());



	@Given("^the app is Installed$")
	public void the_app_is_installed() throws Exception {
		System.out.println("Login to web application...");
		webloginPage.logIn();
		Thread.sleep(5000);
	//	loginPage.update_app();
	}
	
	
	@Given ("^the app is Installed and login as shippercarrier$")
		public void the_app_Installed_and_login_as_shipper_carrier() throws Exception {
			System.out.println("Login to web application...");
			WebLoginPage.logInCarrier();
			CommonFunctions.waitUntilPageLoads();
	}
	
	
	@Then("^I verify Sign In Title for app$")
	public void i_verify_sign_in_title_app() throws InterruptedException {
		loginPage.Verify_Signin_Title();
		System.out.println("Title is Verified");

		}
	@When("^I login to app$")
	public void Login() throws InterruptedException, IOException {
		loginPage.Login();
	}
	
	@Then("^I verify app is in home page$")
	public void verify_homepage() throws InterruptedException, IOException {
		loginPage.verify_homePage();
		System.out.println("App is in Home Page");
	}
	
	@Then("^I get shipments status$")
	public void shipments_availability() {
		
		
	}
	@Given("^the app is launched in Home page$")
	public void launch_in_homePage() throws InterruptedException, IOException {
//		CommonLib.launch_app();
		verify_homepage();
	}
}
